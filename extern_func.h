/* 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 作者              日期      
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 魏彬       		2014年7月30日	
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 描述
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* extern声明了工程中用到的部分函数。
**********************************************************************/
#ifndef __EXTERN_FUNC_H
#define __EXTERN_FUNC_H

#include "Port_init.h"
#include "p32mx150f128b.h"
#include <plib.h>

//***********************port control部分************************//
extern void initPORTs (void);

//***********************TIMER部分************************//
extern void initTimer1();
extern void initTimer2();
extern void initTimer3();
extern void initTimer4();

//***********************UART部分************************//
extern void initUART1 (unsigned int U1_BRATE);

extern void delay (unsigned int delay_n);
extern void putstringUART1( char *s);
extern void putcharUART1(unsigned char c);
extern void putarrayUART1(unsigned char array[],int n);
extern void UART1_Put_Num(unsigned int dat);
extern void UART1_Put_Inf(char *inf, unsigned short int dat);
extern void u32tostr( unsigned int dat,char *str,char *invstr);
extern unsigned int strtou32(char *str);
extern void hextostr(unsigned int dat,char *str);
extern void UART1_Put_hex(unsigned int dat);
extern void SendDecimalismDta(int data);
//***********************ACM部分************************//
extern void ACMint_init();
extern unsigned char initACM(void);
extern unsigned char initACM1(void);
extern unsigned char disableACM(void);
extern unsigned char ACM_Read(unsigned char, unsigned char, unsigned char*);
extern unsigned char ACM_Write(unsigned char, unsigned char, unsigned char*);

extern unsigned char ACM_data_read(void);
extern unsigned char ACM_orientation_read(void);
extern unsigned char ACM_motion_read(void);
extern unsigned char ACM_trans_read(void);
extern unsigned char ACM_mode_read(void);
extern unsigned char ACM_INT_read(void);
extern void ACM_INT_Process(void);

extern unsigned char ACM_STATYS_BUF;
extern unsigned char ACM_MOTION_BUF;
extern unsigned char ACM_ORIENTATION_BUF;
extern unsigned char ACM_TRANS_BUF;
extern unsigned char ACM_MODE_BUF;
extern unsigned char ACM_INT_BUF;
extern unsigned short int ACM_DATA_BUF[3];
extern unsigned char Sys_State_ACM_New;
extern unsigned char ACM_RESULT;
enum ORIENTATION {
    PUF, PUB, PDF, PDB, LRF, LRB, LLF, LLB
}; //方向识别标志
extern enum ORIENTATION Orientation,Orientation_Bef;
//***********************RTCC部分************************//
extern void readRTCC (unsigned char*);
extern void setRTCCtime(unsigned char*);
extern void printSystemTime(void);
extern void printTimeStamp(unsigned char*);
extern void initRTCC(unsigned int, unsigned int);
extern unsigned char RTCCtime[8];
extern unsigned int RTCC_Previous_Reading[3];

//***********************SPI部分************************//
extern void initSPI2_Master(void);
extern void initSPI2_Master_EB(void);
extern void initSPI2_Master_EB_Disable(void);
extern void disableSPI2(void);
extern void PutcharSPI2_DATA(unsigned char c);
extern void PutarraySPI2_DATA(unsigned char array[],int n);
extern void PutcharSPI2_Command(unsigned char c);
extern void PutarraySPI2_Command(unsigned char array[],int n);

extern void initSPI1_Master(void);
extern void initSPI1_Master_EB(void);
extern void initSPI1_Master_EB_Disable(void);
extern void disableSPI1(void);
extern void PutcharSPI1_DATA(unsigned char c);
extern void PutarraySPI1_DATA(unsigned char array[],int n);
//***********************OLED部分************************//
extern void Init_OLED();
//extern unsigned char SpO2_Num[10];
extern unsigned char Default_SpO2[128*6];
extern void Clear_SpO2_Num();
extern void Clear_BPM_Num();
extern void LED_Display_Default();
extern void LED_Display_Dynamic(unsigned char ucIdxX,unsigned char ucIdxY,unsigned char *ucDataStr);
extern void LED_Display_Default_BP();
extern void LED_Display_Dynamic_BP(unsigned char ucIdxX,unsigned char ucIdxY,unsigned char *ucDataStr);
extern void LED_Display_Dynamic_BP_Inv(unsigned char ucIdxX,unsigned char ucIdxY,unsigned char *ucDataStr);
extern void LED_Display_ClearMain();
extern void LED_Display_Perfusion_Index(float wave, float min_wave);
extern void LED_Display_Dynamic_Vertical_PRbpm_BP(unsigned char ucIdxX, unsigned char ucIdxY, unsigned char *ucDataStr);
extern void LED_Display_Dynamic_Vertical_SPO2(unsigned char ucIdxX, unsigned char ucIdxY, unsigned char *ucDataStr); 
extern void LED_Display_Change(void);
extern unsigned char SPO2_OLED_display; //用于血氧显示
extern unsigned char Pulse_rate_OLED_display; //用于脉率血氧显示 
extern unsigned char OLEDSPO2_cnt;
extern unsigned char WaveIdx;
extern float f_HP;
extern float f_HP_min;
extern unsigned char Pulse_Num[10];
extern unsigned char SpO2_Num[10];
extern unsigned char No_Finger[768];
//***********************AFE部分************************//
extern void AFE44x0_Reg_Write(unsigned char R_Address, unsigned int R_Data);
extern unsigned int AFE44x0_Reg_Read(unsigned char R_Address);
extern void Enable_AFE44x0_SPI_Read();
extern void Disable_AFE44x0_SPI_Read();
extern void AFE_init();
extern void AFE_Interrupt_init();
//***********************节能部分************************//
extern void GotoIdle();
extern void GotoSleep();
//***********************数据部分************************//
extern unsigned int SpO2_LP;
extern unsigned int Pulse_rate_LP;
extern unsigned short int SintWave;
extern void SpO2_Pulse_Display_avg_fun(void);
//***********************SW************************//
extern void SW_Interrupt_init();
extern void SW_Interrupt();
extern unsigned char BPx[256];
extern unsigned char SpO2x[256];
extern unsigned char SleepFlag;
extern unsigned char NewBPdata;
extern unsigned char LowmmHg;
extern unsigned short int HimmHg;
extern unsigned short int mmHg;
extern unsigned char PR_BP;
extern unsigned char BPH_Num[10];
extern unsigned char BPL_Num[10];
extern unsigned char mmHg_Num[10];
extern unsigned char Pulse_N[10];
extern unsigned char SPO2orBP_mmHg_cnt;
extern unsigned int Pulse_rate_OLED;
extern unsigned int SPO2_OLED;	
extern unsigned char SPO2orBP;
extern unsigned char SPO2orBP_Bef;
extern unsigned int pulseStarted ;
extern unsigned int IR_LP;
extern unsigned char ReadI2C2Flag;
extern unsigned char AllData[7];
extern unsigned char WaveData[5];
extern unsigned char SpO2Data[5];
extern unsigned char BefSleepFlag;

extern unsigned int motion_flag;
extern void Comm_Process();


#endif