#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/SpO2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/SpO2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../ACM_Funcs.c ../AFE4490.c ../Comm.c ../I2C_Funcs.c ../main.c ../OLED_Funcs.c ../Peripherial_init.c ../RTCC_Funcs.c ../Save_Power.c ../SPI_Funcs.c ../SW.c ../Timer_Funcs.c ../UART_Func.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1472/ACM_Funcs.o ${OBJECTDIR}/_ext/1472/AFE4490.o ${OBJECTDIR}/_ext/1472/Comm.o ${OBJECTDIR}/_ext/1472/I2C_Funcs.o ${OBJECTDIR}/_ext/1472/main.o ${OBJECTDIR}/_ext/1472/OLED_Funcs.o ${OBJECTDIR}/_ext/1472/Peripherial_init.o ${OBJECTDIR}/_ext/1472/RTCC_Funcs.o ${OBJECTDIR}/_ext/1472/Save_Power.o ${OBJECTDIR}/_ext/1472/SPI_Funcs.o ${OBJECTDIR}/_ext/1472/SW.o ${OBJECTDIR}/_ext/1472/Timer_Funcs.o ${OBJECTDIR}/_ext/1472/UART_Func.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1472/ACM_Funcs.o.d ${OBJECTDIR}/_ext/1472/AFE4490.o.d ${OBJECTDIR}/_ext/1472/Comm.o.d ${OBJECTDIR}/_ext/1472/I2C_Funcs.o.d ${OBJECTDIR}/_ext/1472/main.o.d ${OBJECTDIR}/_ext/1472/OLED_Funcs.o.d ${OBJECTDIR}/_ext/1472/Peripherial_init.o.d ${OBJECTDIR}/_ext/1472/RTCC_Funcs.o.d ${OBJECTDIR}/_ext/1472/Save_Power.o.d ${OBJECTDIR}/_ext/1472/SPI_Funcs.o.d ${OBJECTDIR}/_ext/1472/SW.o.d ${OBJECTDIR}/_ext/1472/Timer_Funcs.o.d ${OBJECTDIR}/_ext/1472/UART_Func.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1472/ACM_Funcs.o ${OBJECTDIR}/_ext/1472/AFE4490.o ${OBJECTDIR}/_ext/1472/Comm.o ${OBJECTDIR}/_ext/1472/I2C_Funcs.o ${OBJECTDIR}/_ext/1472/main.o ${OBJECTDIR}/_ext/1472/OLED_Funcs.o ${OBJECTDIR}/_ext/1472/Peripherial_init.o ${OBJECTDIR}/_ext/1472/RTCC_Funcs.o ${OBJECTDIR}/_ext/1472/Save_Power.o ${OBJECTDIR}/_ext/1472/SPI_Funcs.o ${OBJECTDIR}/_ext/1472/SW.o ${OBJECTDIR}/_ext/1472/Timer_Funcs.o ${OBJECTDIR}/_ext/1472/UART_Func.o

# Source Files
SOURCEFILES=../ACM_Funcs.c ../AFE4490.c ../Comm.c ../I2C_Funcs.c ../main.c ../OLED_Funcs.c ../Peripherial_init.c ../RTCC_Funcs.c ../Save_Power.c ../SPI_Funcs.c ../SW.c ../Timer_Funcs.c ../UART_Func.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/SpO2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX150F128B
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1472/ACM_Funcs.o: ../ACM_Funcs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/ACM_Funcs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/ACM_Funcs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ACM_Funcs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/ACM_Funcs.o.d" -o ${OBJECTDIR}/_ext/1472/ACM_Funcs.o ../ACM_Funcs.c  
	
${OBJECTDIR}/_ext/1472/AFE4490.o: ../AFE4490.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/AFE4490.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/AFE4490.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/AFE4490.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/AFE4490.o.d" -o ${OBJECTDIR}/_ext/1472/AFE4490.o ../AFE4490.c  
	
${OBJECTDIR}/_ext/1472/Comm.o: ../Comm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Comm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Comm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Comm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/Comm.o.d" -o ${OBJECTDIR}/_ext/1472/Comm.o ../Comm.c  
	
${OBJECTDIR}/_ext/1472/I2C_Funcs.o: ../I2C_Funcs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/I2C_Funcs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/I2C_Funcs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/I2C_Funcs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/I2C_Funcs.o.d" -o ${OBJECTDIR}/_ext/1472/I2C_Funcs.o ../I2C_Funcs.c  
	
${OBJECTDIR}/_ext/1472/main.o: ../main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/main.o.d" -o ${OBJECTDIR}/_ext/1472/main.o ../main.c  
	
${OBJECTDIR}/_ext/1472/OLED_Funcs.o: ../OLED_Funcs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/OLED_Funcs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/OLED_Funcs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/OLED_Funcs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/OLED_Funcs.o.d" -o ${OBJECTDIR}/_ext/1472/OLED_Funcs.o ../OLED_Funcs.c  
	
${OBJECTDIR}/_ext/1472/Peripherial_init.o: ../Peripherial_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Peripherial_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Peripherial_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Peripherial_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/Peripherial_init.o.d" -o ${OBJECTDIR}/_ext/1472/Peripherial_init.o ../Peripherial_init.c  
	
${OBJECTDIR}/_ext/1472/RTCC_Funcs.o: ../RTCC_Funcs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/RTCC_Funcs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/RTCC_Funcs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/RTCC_Funcs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/RTCC_Funcs.o.d" -o ${OBJECTDIR}/_ext/1472/RTCC_Funcs.o ../RTCC_Funcs.c  
	
${OBJECTDIR}/_ext/1472/Save_Power.o: ../Save_Power.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Save_Power.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Save_Power.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Save_Power.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/Save_Power.o.d" -o ${OBJECTDIR}/_ext/1472/Save_Power.o ../Save_Power.c  
	
${OBJECTDIR}/_ext/1472/SPI_Funcs.o: ../SPI_Funcs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/SPI_Funcs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/SPI_Funcs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/SPI_Funcs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/SPI_Funcs.o.d" -o ${OBJECTDIR}/_ext/1472/SPI_Funcs.o ../SPI_Funcs.c  
	
${OBJECTDIR}/_ext/1472/SW.o: ../SW.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/SW.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/SW.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/SW.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/SW.o.d" -o ${OBJECTDIR}/_ext/1472/SW.o ../SW.c  
	
${OBJECTDIR}/_ext/1472/Timer_Funcs.o: ../Timer_Funcs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Timer_Funcs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Timer_Funcs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Timer_Funcs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/Timer_Funcs.o.d" -o ${OBJECTDIR}/_ext/1472/Timer_Funcs.o ../Timer_Funcs.c  
	
${OBJECTDIR}/_ext/1472/UART_Func.o: ../UART_Func.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/UART_Func.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/UART_Func.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/UART_Func.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/UART_Func.o.d" -o ${OBJECTDIR}/_ext/1472/UART_Func.o ../UART_Func.c  
	
else
${OBJECTDIR}/_ext/1472/ACM_Funcs.o: ../ACM_Funcs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/ACM_Funcs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/ACM_Funcs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ACM_Funcs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/ACM_Funcs.o.d" -o ${OBJECTDIR}/_ext/1472/ACM_Funcs.o ../ACM_Funcs.c  
	
${OBJECTDIR}/_ext/1472/AFE4490.o: ../AFE4490.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/AFE4490.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/AFE4490.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/AFE4490.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/AFE4490.o.d" -o ${OBJECTDIR}/_ext/1472/AFE4490.o ../AFE4490.c  
	
${OBJECTDIR}/_ext/1472/Comm.o: ../Comm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Comm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Comm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Comm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/Comm.o.d" -o ${OBJECTDIR}/_ext/1472/Comm.o ../Comm.c  
	
${OBJECTDIR}/_ext/1472/I2C_Funcs.o: ../I2C_Funcs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/I2C_Funcs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/I2C_Funcs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/I2C_Funcs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/I2C_Funcs.o.d" -o ${OBJECTDIR}/_ext/1472/I2C_Funcs.o ../I2C_Funcs.c  
	
${OBJECTDIR}/_ext/1472/main.o: ../main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/main.o.d" -o ${OBJECTDIR}/_ext/1472/main.o ../main.c  
	
${OBJECTDIR}/_ext/1472/OLED_Funcs.o: ../OLED_Funcs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/OLED_Funcs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/OLED_Funcs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/OLED_Funcs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/OLED_Funcs.o.d" -o ${OBJECTDIR}/_ext/1472/OLED_Funcs.o ../OLED_Funcs.c  
	
${OBJECTDIR}/_ext/1472/Peripherial_init.o: ../Peripherial_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Peripherial_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Peripherial_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Peripherial_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/Peripherial_init.o.d" -o ${OBJECTDIR}/_ext/1472/Peripherial_init.o ../Peripherial_init.c  
	
${OBJECTDIR}/_ext/1472/RTCC_Funcs.o: ../RTCC_Funcs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/RTCC_Funcs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/RTCC_Funcs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/RTCC_Funcs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/RTCC_Funcs.o.d" -o ${OBJECTDIR}/_ext/1472/RTCC_Funcs.o ../RTCC_Funcs.c  
	
${OBJECTDIR}/_ext/1472/Save_Power.o: ../Save_Power.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Save_Power.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Save_Power.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Save_Power.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/Save_Power.o.d" -o ${OBJECTDIR}/_ext/1472/Save_Power.o ../Save_Power.c  
	
${OBJECTDIR}/_ext/1472/SPI_Funcs.o: ../SPI_Funcs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/SPI_Funcs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/SPI_Funcs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/SPI_Funcs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/SPI_Funcs.o.d" -o ${OBJECTDIR}/_ext/1472/SPI_Funcs.o ../SPI_Funcs.c  
	
${OBJECTDIR}/_ext/1472/SW.o: ../SW.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/SW.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/SW.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/SW.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/SW.o.d" -o ${OBJECTDIR}/_ext/1472/SW.o ../SW.c  
	
${OBJECTDIR}/_ext/1472/Timer_Funcs.o: ../Timer_Funcs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Timer_Funcs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Timer_Funcs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Timer_Funcs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/Timer_Funcs.o.d" -o ${OBJECTDIR}/_ext/1472/Timer_Funcs.o ../Timer_Funcs.c  
	
${OBJECTDIR}/_ext/1472/UART_Func.o: ../UART_Func.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/UART_Func.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/UART_Func.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/UART_Func.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I".." -I"." -MMD -MF "${OBJECTDIR}/_ext/1472/UART_Func.o.d" -o ${OBJECTDIR}/_ext/1472/UART_Func.o ../UART_Func.c  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/SpO2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_ICD3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/SpO2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}       -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,-L"F:/Program Files/Microchip/MPLAB C32 Suite/lib",-L"F:/Program Files/Microchip/MPLAB C32 Suite/pic32mx/lib",-L".",-Map="${DISTDIR}/SpO2.X.${IMAGE_TYPE}.map" 
else
dist/${CND_CONF}/${IMAGE_TYPE}/SpO2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/SpO2.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}       -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),-L"F:/Program Files/Microchip/MPLAB C32 Suite/lib",-L"F:/Program Files/Microchip/MPLAB C32 Suite/pic32mx/lib",-L".",-Map="${DISTDIR}/SpO2.X.${IMAGE_TYPE}.map"
	${MP_CC_DIR}\\pic32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/SpO2.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
