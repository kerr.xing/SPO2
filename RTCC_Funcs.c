/* 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 作者              日期      
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 魏彬       		2014年7月30日	
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 描述
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 时钟模块配置
**********************************************************************/

#include "p32mx150f128b.h"
#include "extern_func.h"
#include <stdio.h>
#include <plib.h>
#include "Port_init.h"

//*******************Function declerations *******************//
void readRTCC (unsigned char*);
void setRTCCtime(unsigned char*);
void printSystemTime(void);
void printTimeStamp(unsigned char*);
void initRTCC(unsigned int, unsigned int);
//************************************************************//

unsigned char RTCCtime[8];  //the RTCC time array
                            //RTCCtime[0]=year
                            //RTCCtime[1]=month
                            //RTCCtime[2]=date
                            //RTCCtime[3]=weekday
                            //RTCCtime[4]=hour
                            //RTCCtime[5]=minute
                            //RTCCtime[6]=second
                            //RTCCtime[7]=millisecond (1/256 second)

unsigned int RTCC_Previous_Reading[3]; //the previous RTCC reading. [2]=ms,[1]=RTCTIME,[0]=RTCDATE,
                                            //Since the ms is from the Timer1, it may not well syncronized
                                            //with the RTCC, the shifting from 0xFF to 0x00 may different from the RTCC second
                                            //reading shifting. So we need to use this for a correction.
                                            //for example: 03s 0xFFms may followed by 03s 0x00ms, which is an earlier time.


//******************RTCC (real time clock and calendar) init*************//

void initRTCC(unsigned int Init_Date, unsigned int Init_Time)
{
	IEC0CLR=0x40000000;  //IEC1CLR=0x00008000;  //disable RTCC interrrupts   //WEB
	RTCALRMCLR=0xFFFF;   //Disable alarm //clear ALRMEN, CHIME, AMASK and ARPT
	
	SYSKEY=0xAA996655;   //unlock the crucial register writting sequence lock
	SYSKEY=0x556699AA;   //unlock the crucial register writting sequence lock
	RTCCONSET = 0x0008;		//RTC值可由用户写入
	RTCCONCLR=0x8000;  //clear the RTCCON
	RTCCONSET = 0x0600;	//RTCC由外部辅助时钟输入
//	RTCCONSET=0x00000048;  //set the RTCCON value		//web
	//SYSKEY=0x0;          //lock the crucial register writting sequence lock
	
	while(RTCCON&0x40);  //wait for the RTCC clock to be turned off
	
	RTCTIME=Init_Time;
	RTCDATE=Init_Date;
	
	initTimer1();

/*	T1CON=0;               //clear TxCON register
	TMR1=0;                //clear timer
	T1CONbits.TCKPS=0b10;  //timer input clock divider, 0b11=1/256
	T1CONbits.TCS=1;       //external clock input
	T1CONbits.TSYNC=1;     //sync with external clock input, 32.768 kHz
	IEC0bits.T1IE=0;       //disable Timer interrupts
	 //T1 provide the miliseconde level time stamp. T1 interrupt will not be used in this firmware
	 //TMR1 lower 8 bits will provide the time stamp with a unit=1/256 second
	
	T1CONbits.ON=1;        //Enable Timer 1
*/	
	RTCCONSET=0x00008000;   //start the RTCC
	
	RTCCONCLR=0x00000008;   //disable the RTCTIME and RTCDATE write in
	
	while(!(RTCCON&0x40));   //wait for the RTCC clock to be turned on
}


//***************************RTCC funcs**********************//

//the input of this function should be a array with 8 unsigned char elements//
void readRTCC (unsigned char* TimeArray)
{
	unsigned int timebuf;
	unsigned int datebuf;
	unsigned char msbuf;
	TimeArray=TimeArray+7;
	while (RTCCONbits.RTCSYNC);  //wait for the safe reading window
	
	timebuf=RTCTIME;
	datebuf=RTCDATE;
/*	msbuf=TMR1>>1;

	if(msbuf<RTCC_Previous_Reading[2])  //ms bytes shifted		//这个干什么的？看不懂
	{
		if(timebuf==RTCC_Previous_Reading[1])  //the sync problem between TMR1 and RTCC detected
		{
			msbuf=0xFF;  //correct the sync error by forcing the MS byte to 0xFF
		}
	}
	RTCC_Previous_Reading[2]=msbuf;
	RTCC_Previous_Reading[1]=timebuf;
*/ 
	*TimeArray--=msbuf;    //millisecond (unit as 1/256 second) (Timer1 runs at 512 Hz)
	
	timebuf=timebuf>>8;
	*TimeArray--=timebuf;  //Second
	timebuf=timebuf>>8;
	*TimeArray--=timebuf;  //Minute
	*TimeArray--=timebuf>>8;  //Hour

	*TimeArray--=datebuf;  //Weekday
	datebuf=datebuf>>8;
	*TimeArray--=datebuf;  //Day
	datebuf=datebuf>>8;
	*TimeArray--=datebuf;  //Month
	*TimeArray=datebuf>>8;  //year
}


//void readRTCC (unsigned char* TimeArray)
//{
// unsigned int timebuf;
// while (RTCCONbits.RTCSYNC);  //wait for the safe reading window
// timebuf=RTCDATE;
// *TimeArray++=timebuf>>24; //year
// *TimeArray++=timebuf>>16; //Month
// *TimeArray++=timebuf>>8;  //Day
// *TimeArray++=timebuf;     //Weekday
// timebuf=RTCTIME;
// *TimeArray++=timebuf>>24; //Hour
// *TimeArray++=timebuf>>16; //Minute
// *TimeArray++=timebuf>>8;  //Second
// *TimeArray=TMR1>>1;    //millisecond (unit as 1/256 second) (Timer1 runs at 512 Hz)
// 
//}

//******************************************//
void setRTCCtime(unsigned char* TimeArray)
{
	unsigned int timebuf=0;
	unsigned int datebuf=0;
	
	datebuf=TimeArray[0];            //year
	datebuf=datebuf<<8;
	datebuf=datebuf+TimeArray[1];    //month
	datebuf=datebuf<<8;
	datebuf=datebuf+TimeArray[2];    //date
	datebuf=datebuf<<8;
	datebuf=datebuf+TimeArray[3];    //weekday
	
	timebuf=TimeArray[4];            //hour
	timebuf=timebuf<<8;
	timebuf=timebuf+TimeArray[5];    //minute
	timebuf=timebuf<<8;
	timebuf=timebuf+TimeArray[6];    //second
	timebuf=timebuf<<8;
	
	SYSKEY=0xAA996655;   //unlock the crucial register writting sequence lock
	SYSKEY=0x556699AA;   //unlock the crucial register writting sequence lock
	RTCCONSET=0x00000008;  //set the RTCCON value
	SYSKEY=0x0;          //lock the crucial register writting sequence lock
	RTCTIME=timebuf;   //update time
	RTCDATE=datebuf;   //update date
	TMR1=TimeArray[7];               //update millisecond
	
	RTCCONCLR=0x00000008;	//不可写入值
}


//*************************************//
/*
void printSystemTime(void)
{
	putstringUART5_DM("\r\n_____________system time________________\r\n");
	readRTCC(RTCCtime);
	puthexUART5_DM(RTCCtime[0]);
	puthexUART5_DM(RTCCtime[1]);
	puthexUART5_DM(RTCCtime[2]);
	puthexUART5_DM(RTCCtime[3]);
	puthexUART5_DM(RTCCtime[4]);
	puthexUART5_DM(RTCCtime[5]);
	puthexUART5_DM(RTCCtime[6]);
	puthexUART5_DM(RTCCtime[7]);
	putstringUART5_DM("\r\n________________________________________\r\n");
}
*/
//*************************************//
/*
void printTimeStamp(unsigned char* time_stamp_temp)
{
	puthexUART5_DM(time_stamp_temp[0]);
	puthexUART5_DM(time_stamp_temp[1]);
	puthexUART5_DM(time_stamp_temp[2]);
	puthexUART5_DM(time_stamp_temp[3]);
	puthexUART5_DM(time_stamp_temp[4]);
	puthexUART5_DM(time_stamp_temp[5]);
	puthexUART5_DM(time_stamp_temp[6]);
	puthexUART5_DM(time_stamp_temp[7]);
} */