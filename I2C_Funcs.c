/* 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 作者              日期      
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 魏彬       		2014年7月30日	
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 描述
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* I2C模块配置。
**********************************************************************/

#include "p32mx150f128b.h"
#include "extern_func.h"
#include <stdio.h>
#include <plib.h>
#include "Port_init.h"

#define I2C_TimeOut 10000			//	****???

unsigned char start_I2C1(void);
unsigned char restart_I2C1(void);
unsigned char stop_I2C1(void);
unsigned char ack_I2C1(void);
unsigned char nack_I2C1(void);
unsigned char putchar_I2C1(unsigned char);
unsigned char getchar_I2C1(void);

unsigned char start_I2C2(void);
unsigned char restart_I2C2(void);
unsigned char stop_I2C2(void);
unsigned char ack_I2C2(void);
unsigned char nack_I2C2(void);
unsigned char putchar_I2C2(unsigned char);
unsigned char getchar_I2C2(void);

void initI2C1(void);
void disableI2C1(void);
void initI2C2(void);
void disableI2C2(void);


//****************************I2C1 init******************//
/*
初始化I2C1，用于加速度传感器的通讯
波特率为400kHz
*/
void initI2C1(void)
{
 	I2C1CONCLR=0xFFFFFFFF;
	I2C1STATCLR=0xFFFFFFFF;
	I2C1BRG = 18; 	//3，400kHz 4MHz sysclk  4MHz PBclk
					//18,100kHz
	I2C1CONbits.DISSLW = 1;	//禁止压摆率控制
 	IFS1bits.I2C1MIF=0;
 	IFS1bits.I2C1BIF=0;  //clear the collision flag
 	IEC1bits.I2C1MIE=0;  //I2C2 master interrupt

 	I2C1CONSET=0x00008000;
}

//****************************disable I2C1******************//
void disableI2C1(void)
{
 	I2C1CONCLR=0xFFFFFFFF;
 	I2C1STATCLR=0xFFFFFFFF;

 	IFS1bits.I2C1MIF=0;
 	IFS1bits.I2C1BIF=0;  //clear the collision flag
 	IEC1bits.I2C1MIE=0;  //I2C1 master interrupt
}

//**************************************************//
unsigned char start_I2C1(void)
{
	while(I2C1STATbits.TRSTAT);			//等待总线发送完成
	I2C1CONbits.SEN=1;     //start
 	unsigned short i=0;
 	unsigned char I2C_Err=1;  //I2C error indicator, 1=error, 0= no error
 	for (;i<I2C_TimeOut;i++)
 	{
  		if (!I2C1CONbits.SEN)
  		{
   			I2C_Err=0;
   			break;
  		}
 	}
 	return I2C_Err;
}

//**************************************************//
unsigned char restart_I2C1(void)
{
	while(I2C1STATbits.TRSTAT);			//等待总线发送完成
 	I2C1CONbits.RSEN=1;         //restart
 	unsigned short i=0;
 	unsigned char I2C_Err=1;  //I2C error indicator, 1=error, 0= no error
 	for (;i<I2C_TimeOut;i++)
 	{
  		if (!I2C1CONbits.RSEN)
  		{
   			I2C_Err=0;
   			break;
  		}
 	}
 	return I2C_Err;
}

//**************************************************//
unsigned char stop_I2C1(void)
{
 	I2C1CONbits.PEN=1;   //stop
 	unsigned short i=0;
 	unsigned char I2C_Err=1;  //I2C error indicator, 1=error, 0= no error
 	for (;i<I2C_TimeOut;i++)
 	{
 		if (!I2C1CONbits.PEN)
  		{
   			I2C_Err=0;
   			break;
  		}
 	}
 	return I2C_Err;
}

//**************************************************//
unsigned char ack_I2C1(void)
{
 	I2C1CONbits.ACKDT=0;   //set acknoledge
 	I2C1CONbits.ACKEN=1;   //not ack
 	unsigned short i=0;
 	unsigned char I2C_Err=1;  //I2C error indicator, 1=error, 0= no error
 	for (;i<I2C_TimeOut;i++)
 	{
  		if (!I2C1CONbits.ACKEN)
  		{
   			I2C_Err=0;
   			break;
  		}
 	}
 	return I2C_Err;
}

//**************************************************//
unsigned char nack_I2C1(void)
{
 	I2C1CONbits.ACKDT=1;   //set not acknoledge
 	I2C1CONbits.ACKEN=1;   //not ack
 	unsigned short i=0;
 	unsigned char I2C_Err=1;  //I2C error indicator, 1=error, 0= no error
 	for (;i<I2C_TimeOut;i++)
 	{
  		if (!I2C1CONbits.ACKEN)
  		{
   			I2C_Err=0;
   			break;
  		}
 	}
 	return I2C_Err;
}

//**************************************************//
unsigned char putchar_I2C1(unsigned char I2Cdata)
{
 	while(I2C1STATbits.TRSTAT);			//等待总线发送完成
	I2C1TRN=I2Cdata;   //send data
 	unsigned short i=0;
 	unsigned char I2C_Err=1;  //I2C error indicator, 1=error, 0= no error
 	for (;i<I2C_TimeOut;i++)
 	{
  		if (!I2C1STATbits.TRSTAT)
  		{
   			break;
  		}
 	}
 	i=0;
 	for (;i<I2C_TimeOut;i++)
 	{
 	 	if (!I2C1STATbits.ACKSTAT)
  		{
		   I2C_Err=0;
		   break;
  		}
 	}
 	return I2C_Err;

}

//**************************************************//
unsigned char getchar_I2C1(void)
{
 	I2C1CONbits.RCEN=1;         //receive
 	unsigned short i=0;
 	for (;i<I2C_TimeOut;i++)
 	{
  		if (!I2C1CONbits.RCEN)
  		{
   			break;
  		}
 	}
 	return I2C1RCV;
}




//****************************I2C2 init******************//
/*
初始化I2C2，用于血压/血氧通讯
波特率为100kHz
*/
void initI2C2(void)
{
	ANSELBCLR = 0x000C;
 	I2C2BRG=19;  //100kHz 4MHz PBclock 	2 400kHz
	I2C2CONSET=0x00000200;	//禁止压摆率控制
//	I2C2CONbits.GCEN = 1;
	I2C2ADD = 0x50;	//对应与读写分别为0xA0,0xA1

 	IFS1bits.I2C2MIF=0;
 	IFS1bits.I2C2BIF=0;  //clear the collision flag
	IFS1bits.I2C2SIF=0;
 	IEC1bits.I2C2MIE=0;  //I2C2 master interrupt
	IEC1bits.I2C2SIE=0;  //I2C2 slave interrupt
	IEC1bits.I2C2BIE=0;  //I2C2 bus interrupt
 	IPC9bits.I2C2IP=4;  //priority
 	IPC9bits.I2C2IS=3;  //sub-priority
	
 	I2C2CONSET=0x00008000;
	IEC1bits.I2C2SIE=1;  //I2C2 slave interrupt
	IEC1bits.I2C2BIE=0;
}

//****************************disable I2C2******************//
void disableI2C2(void)
{
 	I2C2CONCLR=0xFFFFFFFF;
 	I2C2STATCLR=0xFFFFFFFF;

 	IFS1bits.I2C2MIF=0;
 	IFS1bits.I2C2BIF=0;  //clear the collision flag
	IFS1bits.I2C2SIF=0;
 	IEC1bits.I2C2MIE=0;  //I2C2 master interrupt
	IEC1bits.I2C2SIE=0;  //I2C2 slave interrupt
}

//**************************************************//
unsigned char start_I2C2(void)
{
	I2C2CONbits.SEN=1;     //start
 	unsigned short i=0;
 	unsigned char I2C_Err=1;  //I2C error indicator, 1=error, 0= no error
 	for (;i<I2C_TimeOut;i++)
 	{
  		if (!I2C2CONbits.SEN)
  		{
   			I2C_Err=0;
   			break;
  		}
 	}
 	return I2C_Err;
}

//**************************************************//
unsigned char restart_I2C2(void)
{
 	I2C2CONbits.RSEN=1;         //restart
 	unsigned short i=0;
 	unsigned char I2C_Err=1;  //I2C error indicator, 1=error, 0= no error
 	for (;i<I2C_TimeOut;i++)
 	{
  		if (!I2C2CONbits.RSEN)
  		{
   			I2C_Err=0;
   			break;
  		}
 	}
 	return I2C_Err;
}

//**************************************************//
unsigned char stop_I2C2(void)
{
 	I2C2CONbits.PEN=1;   //stop
 	unsigned short i=0;
 	unsigned char I2C_Err=1;  //I2C error indicator, 1=error, 0= no error
 	for (;i<I2C_TimeOut;i++)
 	{
 		if (!I2C2CONbits.PEN)
  		{
   			I2C_Err=0;
   			break;
  		}
 	}
 	return I2C_Err;
}

//**************************************************//
unsigned char ack_I2C2(void)
{
 	I2C2CONbits.ACKDT=0;   //set acknoledge
 	I2C2CONbits.ACKEN=1;   //not ack
 	unsigned short i=0;
 	unsigned char I2C_Err=1;  //I2C error indicator, 1=error, 0= no error
 	for (;i<I2C_TimeOut;i++)
 	{
  		if (!I2C2CONbits.ACKEN)
  		{
   			I2C_Err=0;
   			break;
  		}
 	}
 	return I2C_Err;
}

//**************************************************//
unsigned char nack_I2C2(void)
{
 	I2C2CONbits.ACKDT=1;   //set not acknoledge
 	I2C2CONbits.ACKEN=1;   //not ack
 	unsigned short i=0;
 	unsigned char I2C_Err=1;  //I2C error indicator, 1=error, 0= no error
 	for (;i<I2C_TimeOut;i++)
 	{
  		if (!I2C2CONbits.ACKEN)
  		{
   			I2C_Err=0;
   			break;
  		}
 	}
 	return I2C_Err;
}

//**************************************************//
unsigned char putchar_I2C2(unsigned char I2Cdata)
{
 	I2C2TRN=I2Cdata;   //send data
 	unsigned short i=0;
 	unsigned char I2C_Err=1;  //I2C error indicator, 1=error, 0= no error
 	for (;i<I2C_TimeOut;i++)
 	{
  		if (!I2C2STATbits.TRSTAT)
  		{
   			break;
  		}
 	}
 	i=0;
 /*	for (;i<I2C_TimeOut;i++)
 	{
 	 	if (!I2C2STATbits.ACKSTAT)
  		{
		   I2C_Err=0;
		   break;
  		}
 	}*/
 	return I2C_Err;

}

//**************************************************//
unsigned char getchar_I2C2(void)
{
 	I2C2CONbits.RCEN=1;         //receive
 	unsigned short i=0;
 	for (;i<I2C_TimeOut;i++)
 	{
  		if (!I2C2CONbits.RCEN)
  		{
   			break;
  		}
 	}
 	return I2C2RCV;
}