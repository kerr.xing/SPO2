/* 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 作者              日期      
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 魏彬       		2014年7月30日	
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 描述
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* UART模块配置
**********************************************************************/

#include "p32mx150f128b.h"
#include "stdio.h"
#include "extern_func.h"
#include "Port_init.h"
#include <plib.h>


#define UART1_MODE 0xA208//BRGH=1，0校验、8位数据，tx空闲为1，1停止位
#define UART1_MODE2 0x8210//BRGH=0，0校验、8位数据，tx空闲为0，1停止位


//*********************函数声明***************//
void initUART1 (unsigned int);

void delay (unsigned int delay_n);
void putstringUART1( char *s);
void putcharUART1(unsigned char c);
void putarrayUART1(unsigned char array[],int n);
void UART1_Put_Num(unsigned int dat);
void UART1_Put_Inf(char *inf, unsigned short int dat);
void u32tostr( unsigned int dat,char *str,char *invstr) ;
unsigned int strtou32(char *str) ;
void hextostr(unsigned int dat,char *str) ;
void UART1_Put_hex(unsigned int dat);
void SendDecimalismDta(int data);

/****************************************
函数名：initUART1 (unsigned int U1_BRATE)
参数：U1_BRATE ：波特率参数。公式：
描述：用于初始化和使能UART1，并配置波特率
**************************************/
void initUART1 (unsigned int U1_BRATE)
{
//	ANSELBSET = 0x000C;
	ANSELBCLR = 0x0004;	//uart rx接收时，可能需要设为数字量
	
//************UART管脚配置*************//
	TxD_SCL_TRIS = 0;
	Rxd_SDA_TRIS = 1;
	CFGCONbits.IOLOCK = 0;
	U1RXRbits.U1RXR = 4;		//RXD映射到RPB2		input
	RPB3Rbits.RPB3R = 1;		//TXD映射到RPB3		output
	
	CFGCONbits.IOLOCK = 1;
//************UART管脚配置*************//

	IFS1bits.U1TXIF = 0;		// 清中断标志位
	IEC1bits.U1TXIE = 0;		// 使能uart中断
	//IPC3bits.U1TXIP = 6;   	// 设置中断优先级
	IFS1bits.U1RXIF = 0;		// 
	IEC1bits.U1RXIE = 0;		// 
	IPC8bits.U1IP = 6;   		//只有U1，没有U1RX/TX,见总手册表7.1		 

	U1BRG=U1_BRATE;
	U1MODE=UART1_MODE;
	U1MODEbits.SIDL = 1;		//器件进入IDLE之后，模块停止工作。
	U1STA=0x0400;		//产生中断方式可设置：Tx有空时/rx有数据时
}



//******************************************//
void delay (unsigned int delay_n)		//delay_n * 1 ms
{
	//int ms_cnt = 2909 * delay_n;
	int ms_cnt = 364 * delay_n;
	while(ms_cnt)
	{
		ms_cnt--;
	}
}
//**********************************************************//

void putstringUART1( char *s)
{
	while( *s)			// loop until *s == '\0' the  end of the string
  	putcharUART1( *s++);	// send the character and point to the next one
    putcharUART1( '\r');       // terminate with a cr / line feed
}

//***********************************************************//
void putcharUART1(unsigned char c)
{
	while ( U1STAbits.UTXBF);   // wait while Tx buffer full

		U1TXREG = c;

} 
//***********************************************************//
void putarrayUART1(unsigned char array[],int n)
{
		int i;
		for(i=0;i<n;i++)
		putcharUART1(array[i]);
}

/*****************************************************************/

void UART1_Put_Num(unsigned int dat)
{
 char temp[20];
char temp1[20];
 u32tostr(dat,temp,temp1);
 putstringUART1(temp);
}

/*******************************************************************/

void UART1_Put_Inf(char *inf, unsigned short int dat)
{ char temp[20];
char temp1[20];
 putstringUART1(inf);
 u32tostr(dat,temp,temp1);
 putstringUART1(temp);
 //putcharUART1(dat/100+0x30);
 //dat=dat%100;
 //putcharUART1(dat/10+0x30);
 //putcharUART1(dat%10+0x30);
 putstringUART1("\r\n");  
}

/******************************************************************/

void u32tostr( unsigned int dat,char *str,char *invstr) 
{
	 char temp[20];
	 unsigned char cnt = 0;
	 unsigned char i=0,j=0;	
/*
   if(dat==0){
    str[0]='0';
    str[1]='\0';
    invstr[0]='0';
    invstr[1]='\0';
    }
	return;*/
	 while(dat)
	 {
	  temp[i]=dat%10+0x30;
	  i++;
	  dat/=10;
	 }
	 j=i;
	 cnt = i;
	 for(i=0;i<j;i++)
	 {
	  str[i]=temp[i];	//web str[i]=temp[j-i-1];
	  invstr[i] = temp[j-i-1];	//高位在前
	 }
	if(i>=3) 			//i>=4
	{
		str[i]='\0';
		invstr[i]='\0';
	}
	else
	{
		for(;i<3;i++)	//i<4
		{
			str[i]=0x3A;
			invstr[i]=0x3A;
		}
		str[i]='\0';
		invstr[i]='\0';
	}
	str[3] = '\0';		//后加web
	invstr[3] = '\0';
 
        
}

/********************************************************************/

unsigned int strtou32(char *str) 
{
 unsigned int temp=0;
 unsigned int fact=1;
 unsigned char len=strlen(str);
 unsigned char i;
 for(i=len;i>0;i--)
 {
  temp+=((str[i-1]-0x30)*fact);
  fact*=10;
 }
 return temp;
}

/******************************************************************/

void hextostr(unsigned int dat,char *str) 
{
 char temp[20];
 unsigned char i=0,j=0;
 i=0;
 while(dat)
 {
  temp[i]=dat%16;
  if (temp[i]>=10)
	temp[i]=temp[i]+0x31;
  temp[i]=temp[i]+0x30;
  i++;
  dat/=16;
 }
 j=i;
 for(i=0;i<j;i++)
 {
  str[i]=temp[j-i-1];
 }
 if(!i) {str[i++]='0';}
 str[i]=0;
}

/*****************************************************************/

void UART1_Put_hex(unsigned int dat)
{
 char temp[20];
 hextostr(dat,temp);
 putstringUART1(temp);
}

//仅用于打印
/**
 * 把char转化为字符
 * @param data char形数据
 * @return 转化的字符
 */
char Get16_Char(unsigned char data) {
    switch (data) {
        case 0: return '0';
        case 1: return '1';
        case 2: return '2';
        case 3: return '3';
        case 4: return '4';
        case 5: return '5';
        case 6: return '6';
        case 7: return '7';
        case 8: return '8';
        case 9: return '9';
        case 10: return 'A';
        case 11: return 'B';
        case 12: return 'C';
        case 13: return 'D';
        case 14: return 'E';
        case 15: return 'F';
    }
    return '$';
}


void SendDecimalismDta(int data) {
    unsigned char stringData[7];
    int i = 0, j = 0;
    unsigned char temp = 0;
    if (data < 0) {
        stringData[0] = '-';
        data = -data;
    } else if (data == 0) {
        putstringUART1("0 ");
        return;
    } else
        stringData[0] = ' ';
    while (data != 0) {
        i++;
        stringData[i] = Get16_Char((unsigned char) (data % 10));
        data /= 10;
    }
    stringData[i + 1] = '\0';
    for (j = 1; j < i; ++j, --i) {
        temp = stringData[j];
        stringData[j] = stringData[i];
        stringData[i] = temp;
    }
    putstringUART1(stringData);
}