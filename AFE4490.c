/* 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 作者              日期      
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 魏彬       		2014年9月2日	
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 描述
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* AFE的初始化和配置
***********************************************************************/

#include "p32mx150f128b.h"
#include "extern_func.h"
#include "Port_init.h"
#include "AFE44x0.h"
#include <stdio.h>
#include <plib.h>
#include <math.h>

void AFE44x0_Reg_Write(unsigned char R_Address, unsigned int R_Data);
unsigned int AFE44x0_Reg_Read(unsigned char R_Address);
void Enable_AFE44x0_SPI_Read();
void Disable_AFE44x0_SPI_Read();
void AFE_init();
void AFE_Interrupt_init();

/*
AFE的初始化配置，包括采样率、内部放大系数、补偿系数等等
*/
void AFE_init()
{
	unsigned int i = 0;
	unsigned int fifo_num = 0;
	unsigned int uData[10];
	RST_AFE = 0;
	STE = 1;					//STE即SPI通讯中的SS信号线，这里采用手动控制
	SPI_CLK = 0;
	PWD_AFE = 0;				//PWD和RST进行一次总体复位
	for(i = 0;i<1000;i++);
	RST_AFE = 1;
	PWD_AFE = 1;
	//进行各个采样控制的时间设置，改时间以PRP为基准，这里设置为200Hz
	AFE44x0_Reg_Write((unsigned char)CONTROL0, 0x000000);            //0x00
	AFE44x0_Reg_Write((unsigned char)PRPCOUNT, 19999);			//200Hz
  	AFE44x0_Reg_Write((unsigned char)LED2STC, 15150);			//AFE44x0_Reg_Write((unsigned char)LED2STC, 15050);
  	AFE44x0_Reg_Write((unsigned char)LED2ENDC, 19998);
  	AFE44x0_Reg_Write((unsigned char)LED2LEDSTC, 15000);
	AFE44x0_Reg_Write((unsigned char)LED2LEDENDC, 19999);
	AFE44x0_Reg_Write((unsigned char)ALED2STC, 50);
	AFE44x0_Reg_Write((unsigned char)ALED2ENDC, 4998);
	AFE44x0_Reg_Write((unsigned char)LED1STC, 5150);			//	AFE44x0_Reg_Write((unsigned char)LED1STC, 5050);
	AFE44x0_Reg_Write((unsigned char)LED1ENDC, 9998);
	AFE44x0_Reg_Write((unsigned char)LED1LEDSTC, 5000);
	AFE44x0_Reg_Write((unsigned char)LED1LEDENDC, 9999);
	AFE44x0_Reg_Write((unsigned char)ALED1STC, 10050);
	AFE44x0_Reg_Write((unsigned char)ALED1ENDC, 14998);
	AFE44x0_Reg_Write((unsigned char)LED2CONVST, 4);
	AFE44x0_Reg_Write((unsigned char)LED2CONVEND, 4999);
	AFE44x0_Reg_Write((unsigned char)ALED2CONVST, 5004);
	AFE44x0_Reg_Write((unsigned char)ALED2CONVEND, 9999);
	AFE44x0_Reg_Write((unsigned char)LED1CONVST, 10004);
	AFE44x0_Reg_Write((unsigned char)LED1CONVEND, 14999);
	AFE44x0_Reg_Write((unsigned char)ALED1CONVST, 15004);
	AFE44x0_Reg_Write((unsigned char)ALED1CONVEND, 19999);
	AFE44x0_Reg_Write((unsigned char)ADCRSTSTCT0, 0);
	AFE44x0_Reg_Write((unsigned char)ADCRSTENDCT0, 3);
	AFE44x0_Reg_Write((unsigned char)ADCRSTSTCT1, 5000);
	AFE44x0_Reg_Write((unsigned char)ADCRSTENDCT1, 5003);
	AFE44x0_Reg_Write((unsigned char)ADCRSTSTCT2, 10000);
	AFE44x0_Reg_Write((unsigned char)ADCRSTENDCT2, 10003);
	AFE44x0_Reg_Write((unsigned char)ADCRSTSTCT3, 15000);
	AFE44x0_Reg_Write((unsigned char)ADCRSTENDCT3, 15003);
	//用于控制、采样的参数配置
	AFE44x0_Reg_Write((unsigned char)CONTROL2, 0x020000);           //0x23
	AFE44x0_Reg_Write((unsigned char)TIAGAIN, (unsigned int)(RF_LED1_500K + CF_LED1_5P_5P + STAGE2EN_LED1 + STG2GAIN_LED1_6DB));// + ENSEPGAIN  ));        //0x20
	AFE44x0_Reg_Write((unsigned char)TIA_AMB_GAIN, (unsigned int)(FLTRCNRSEL_500HZ + AMBDAC_3uA));//RF_LED2_1M + CF_LED2_30P_5P + STG2GAIN_LED2_6DB + STAGE2EN_LED2));       //0x21
//	AFE44x0_Reg_Write((unsigned char)LEDCNTRL, 0x015A80);           //LED1=17.57815mA;LED2=25mA;公式：电流=LEDx[7:0]/256*50mA(01)；
//	AFE44x0_Reg_Write((unsigned char)LEDCNTRL, 0x013040); 	//LED的驱动电流设置，最终产品可能需要再做调整
    AFE44x0_Reg_Write((unsigned char)LEDCNTRL, 0x014B61);  //LED1=14.6843mA;LED2=19mA;
	AFE44x0_Reg_Write((unsigned char)CONTROL1, 0x000103);           //0x1E 
  
  	Enable_AFE44x0_SPI_Read();								//使能寄存器的读.
	fifo_num = SPI1STATbits.RXBUFELM;						//使能后，把fifo中的数据读光。
	for(i = 0;i<fifo_num;i++)
		uData[i] = SPI1BUF;
	SPI1STATbits.SPIROV = 0;
}

/*
AFE中断初始化配置，这里将其中断优先级设为最高。
*/
void AFE_Interrupt_init()
{
	IFS0bits.INT0IF = 0;
	IPC0bits.INT0IP = 5;
	IPC0bits.INT0IS = 2;
	INTCONbits.INT0EP = 1;				//上升沿触发
	IFS0bits.INT0IF = 0;	
	IEC0bits.INT0IE = 1;
}

/*
AFE写寄存器函数。
参数：R_Address:寄存器的地址
	R_Data:配置的数值（低24位有效）
说明：因为AFE的寄存器为24位长度，总体发送类似32位数据发送，但是头8位为地址，后24位为数据
*/
void AFE44x0_Reg_Write(unsigned char R_Address, unsigned int R_Data)
{
	unsigned int Add_Data = 0;
	unsigned int Add32 = 0;
	STE = 0;
	Add32 = R_Address << 24;
	Add_Data = Add32 + R_Data;
	while(!SPI1STATbits.SPITBE);
	SPI1BUF = Add_Data;
	while(!SPI1STATbits.SPITBE);
	while(!SPI1STATbits.SRMT);
	STE = 1;
}

/*
AFE读寄存器函数。
参数：R_Address:寄存器的地址
返回值：retVal:寄存器的数值（低24位有效）
*/
unsigned int AFE44x0_Reg_Read(unsigned char R_Address)
{
	unsigned int Add_Data = 0;
	unsigned int Add32 = 0;
	unsigned int fifo_depth = 0;
	unsigned int retVal = 0;
	unsigned int laVal[4];
	unsigned int i =0;
	STE = 0;
	Add32 = R_Address << 24;
	Add_Data = Add32 + 0x000000;		//低24位仅为了产生时钟而设置，无实际意义
	while(!SPI1STATbits.SPITBE);
	SPI1BUF = Add_Data;
	while(!SPI1STATbits.SPITBE);
	while(!SPI1STATbits.SRMT);
	STE = 1;
	retVal = SPI1BUF;
	fifo_depth = SPI1STATbits.RXBUFELM;	//清空FIFO，以防溢出
	for(i = 0;i<fifo_depth;i++)
		laVal[i] = SPI1BUF;
	return retVal;
}

/*
使能AFE读寄存器函数。
使能读之后，才能将AFE的寄存器值读出。
*/
void Enable_AFE44x0_SPI_Read()
{
	STE = 0;
	while(!SPI1STATbits.SPITBE);
	SPI1BUF = 0x00000001;			//将地址为0x00的寄存器最低位配置为1
	while(!SPI1STATbits.SPITBE);
	while(!SPI1STATbits.SRMT);
	STE = 1;	
}

/*
禁止AFE读寄存器函数。
*/
void Disable_AFE44x0_SPI_Read()
{
	STE = 0;
	while(!SPI1STATbits.SPITBE);
	SPI1BUF = 0x00000000;
	while(!SPI1STATbits.SPITBE);
	while(!SPI1STATbits.SRMT);
	STE = 1;
}


