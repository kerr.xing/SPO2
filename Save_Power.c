#include "p32mx150f128b.h"
#include "extern_func.h"
#include "Port_init.h"
#include "AFE44x0.h"
#include <stdio.h>
#include <plib.h>
#include <math.h>

unsigned char SleepFlag = 0;
/****************************************
函数名：GotoSleep()
描述：进入休眠模式，进入休眠模式前，关闭所有LED、关闭光频转换器
      休眠唤醒后，马上进行系统软复位，重新开始运行程序。
**************************************/
void GotoSleep()
{	
	RST_AFE = 0;
	OLED_RST = 0;
	IEC1bits.I2C2SIE = 0;
	IFS1bits.I2C2SIF = 0;
	SPI2CONbits.ON = 0;
	SPI1CONbits.ON = 0;
	I2C1CONCLR = 0x00008000;
	T4CONCLR = 0x8000;
	IEC0bits.INT4IE = 0;
	IFS0bits.INT4IF = 0;
	IEC0bits.INT0IE = 0;
	IFS0bits.INT0IF = 0;
	IFS0bits.INT1IF = 0;
	IEC0bits.INT1IE = 1;		//INT3允许中断
 	IEC1bits.I2C2MIE=0;  //I2C2 master interrupt
	IEC1bits.I2C2SIE=0;  //I2C2 slave interrupt
	IEC1bits.I2C2BIE=0;  //I2C2 bus interrupt	
	IFS1bits.I2C2MIF=0;
 	IFS1bits.I2C2BIF=0;  //clear the collision flag
	IFS1bits.I2C2SIF=0;
	OLEDSPO2_cnt = 0;    //显示滤波计数清零
	I2C1CONCLR=0x00008000;
	I2C2CONCLR=0x00008000;

	SYSKEY = 0x0;			//写入系统解锁序列
	SYSKEY = 0xAA996655;
	SYSKEY = 0x556699AA;
	OSCCONSET = 0x10;		//可进入SLEEP模式
	SleepFlag = 1;
	SYSKEY = 0x0;
	asm volatile ("wait");	//wait后即休眠，唤醒后，先运行中断，然后从下一句语句开始运行

	WDTCONbits.ON = 1;			//开启看门狗，同时死循环，让
	while(1);					//系统唤醒后，马上复位。

}

/****************************************
函数名：GotoIdle()
描述：进入空闲模式，降低功耗
**************************************/
void GotoIdle()
{	
//	Led = 1;//LATBSET = 0x1000;				//指示一下是否有进idle		
	SYSKEY = 0x0;					//进入IDLE模式
	SYSKEY = 0xAA996655;
	SYSKEY = 0x556699AA;
	OSCCONCLR = 0x10;
	SYSKEY = 0x0;
	asm volatile ("wait");			//wait后进行IDEL模式，由中断唤醒后，执行完对应中断后，从下面开始执行
}
