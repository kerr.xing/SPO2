/* 
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 作者              日期      
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 魏彬       		2014年5月22日	
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 描述
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 最终版，针对PIC32MX150F128B芯片进行程序的重新修改
 ***********************************************************************/

#include "p32mx150f128b.h"
#include "extern_func.h"
#include "Port_init.h"
#include "AFE44x0.h"
#include <stdio.h>
#include <plib.h>
#include <math.h>


#define FSM_IDLE  0x00
#define FSM_MOTION1 0x01
#define FSM_MOTION2 0x02

#define Const3 4000000 	
#define Desampling_rate 4
#define HP_f 31   
#define HP_f_bit 5
#define Hz_upper_threshold 0.9			//光强阈值
#define Hz_lower_threshold 0.01			//光弱阈值
#define no_finger_threshold 400			//差不多8秒
#define Pulse_buffer_size 512	
//RTCC
#define InitTime 0x08000000		//8:00
#define InitDate 0x14010103		//2014年1月1日 星期三

#define sampleRate 50

//函数声明
unsigned int Pulse_rate_fn(); //用于脉率及SpO2计算	
void filter4(); //4点平滑滤波
void ModelSelect(); //发光测量模式选择
void avg_fun();
void Bef_GotoSleep();
void SportFSM(); //运动状态机
void SpO2_Pulse_Display_avg_fun(void);
void send_uart(void);

//BG/IR/RD强度计算				
unsigned int decRD_signal = 0;
unsigned int decIR_signal = 0;
unsigned int RD_Intensity = 0;
unsigned int IR_Intensity = 0;
//用于4点平滑滤波
unsigned char Desampling = 0;
unsigned int RD_LP = 0; //WEB			
unsigned int RD_temp3 = 0;
unsigned int RD_temp2 = 0;
unsigned int RD_temp1 = 0;
unsigned int IR_LP = 0; //WEB
unsigned int IR_temp3 = 0;
unsigned int IR_temp2 = 0;
unsigned int IR_temp1 = 0;

unsigned int Pulse_rate_LP = 0;
unsigned int Pulse_rate_temp3 = 0;
unsigned int Pulse_rate_temp2 = 0;
unsigned int Pulse_rate_temp1 = 0;

unsigned int SpO2_temp1 = 0;
unsigned int SpO2_temp2 = 0;
unsigned int SpO2_temp3 = 0;
unsigned int SpO2_LP = 0;
//pan
unsigned int SpO2_temp7 = 0;
unsigned int SpO2_temp6 = 0;
unsigned int SpO2_temp5 = 0;
unsigned int SpO2_temp4 = 0;
//pan
unsigned int Pulse_rate_temp7 = 0;
unsigned int Pulse_rate_temp6 = 0;
unsigned int Pulse_rate_temp5 = 0;
unsigned int Pulse_rate_temp4 = 0;

unsigned char WaveIdx = 0;
unsigned char SPO2_OLED_display; //用于血氧显示
unsigned char Pulse_rate_OLED_display; //用于脉率血氧显示 
unsigned char OLEDSPO2_cnt = 0; //8次显示滤波缓冲
unsigned char LED_Display_Timer4En = 0; //定时器4显示使能
//血氧显示值缓存
unsigned char SPO2_OLED_temp1 = 0;
unsigned char SPO2_OLED_temp2 = 0;
unsigned char SPO2_OLED_temp3 = 0;
unsigned char SPO2_OLED_temp4 = 0;
unsigned char SPO2_OLED_temp5 = 0;
unsigned char SPO2_OLED_temp6 = 0;
unsigned char SPO2_OLED_temp7 = 0;
//脉率显示值缓存
unsigned char Pulse_rate_OLED_temp1 = 0;
unsigned char Pulse_rate_OLED_temp2 = 0;
unsigned char Pulse_rate_OLED_temp3 = 0;
unsigned char Pulse_rate_OLED_temp4 = 0;
unsigned char Pulse_rate_OLED_temp5 = 0;
unsigned char Pulse_rate_OLED_temp6 = 0;
unsigned char Pulse_rate_OLED_temp7 = 0;
//用于32点平滑滤波后，进行高通运算
unsigned int Pulse_HP_templ = 0;
unsigned int Pulse_HP = 0;
unsigned int Pulse_HP_buffer[32];
unsigned char HB_counter = 0;
//用于32点平滑滤波，得到类正弦波
unsigned int Pulse_HP_temp[16];
unsigned int Pulse_HP_Total_avg = 0;
unsigned int Pulse_HP_Total = 0;
unsigned int fil_counter = 0;
//峰峰值计算及SpO2计算
unsigned int Pulse_buffer_counter = 0;
unsigned int ind_min = 0;
unsigned int ind_max = 0;
unsigned int interval_counter = 0;
unsigned int Pulse_rate = 0;
float IR_buffer[Pulse_buffer_size];
float RD_buffer[Pulse_buffer_size];
unsigned int HP_buffer[Pulse_buffer_size];
float Pulse_amp = 0;
unsigned int Pulse = 0;
unsigned char Set_enable = 0;
unsigned char Reset_enable = 0;
unsigned int SPO2 = 0;
float Amp_threshold = 0.0002;
float SPO2_R = 0;
//计数器及标志位
unsigned char DAQ_enable = 0;
unsigned char LP_Done = 0;
unsigned int invalid_finger = 0;
//模式选择
unsigned int Pulse_HP_MAX = 0;
unsigned int Pulse_HP_MIN = 0;
unsigned int SPO2_pre = 0;
float f_RD = 0;
float f_IR = 0;
float f_HP = 0;
float f_HP_min = 0;
unsigned char *p_RD;
unsigned char *p_IR;
unsigned int WaveCnt = 0;
float maxIRValue = 0;
float minIRValue = 1.5;
float maxRedValue = 0;
float minRedValue = 1.5;
unsigned int pulseStarted = 0;
unsigned int previousValue = 0;
unsigned int currentValue = 0;
unsigned int ppreviousValue = 0;
int transitions[2] = {-1, -1}; //用于峰值检测
unsigned int avg_cnt = 0;
unsigned int filtIRSample = 0;
unsigned int Pulse_weak = 0;
unsigned int motion_flag = 0;
unsigned int preSpO2 = 99;
unsigned int prePulseRate = 0;
unsigned int motion_cnt = 0;
unsigned int FSM_STATE = FSM_IDLE;
unsigned int low_cnt = 0;
unsigned int high_cnt = 0;
unsigned int SwitchCnt = 0;
unsigned char SPO2orBP = 0;
unsigned char SPO2orBP_Bef = 0;

unsigned char I2C2DATA_test = 0;
unsigned char flag_i2c = 0;
unsigned char I2Cdate_array[20];
unsigned char I2Ccnt = 0;
unsigned int int_cnt = 0;
unsigned int int_cnt1 = 0;
unsigned int int_cnt2 = 0;
unsigned int int_cnt3 = 0;
unsigned int int_cnt4 = 0;
//发送和显示的全局变量
unsigned char BPH_Num[10];
unsigned char BPL_Num[10];
unsigned char mmHg_Num[10];
unsigned char Pulse_N[10];
unsigned char Pulse_Num[10];
unsigned char SpO2_Num[10];
unsigned int Pulse_rate_OLED = 0;
unsigned int SPO2_OLED = 0;
unsigned char BefSleepFlag = 0;
unsigned char SPO2orBP_mmHg_cnt = 0;

unsigned int abc_test = 0;
unsigned int abc_test_dc = 0;


//去除AC和DC不合理的值
float irACtmp;
float redACtmp;
float irDCtemp;
float redDCtemp;

unsigned char ACMtemp;

/*
运动剔除状态机，当检测到超过阈值的运动时，则不进行血氧计算，维持上一次值
 */
void SportFSM() {
    switch (FSM_STATE) {
        case FSM_IDLE:
            if (motion_flag) {
                FSM_STATE = FSM_MOTION1;
                motion_cnt++;
                motion_flag = 0;
            } else {
                motion_cnt = 0;
                FSM_STATE = FSM_IDLE;
            }
            break;
        case FSM_MOTION1:
            putstringUART1("AAA\n\r");
            if (motion_cnt <= 60) //大概3s时间
            {
                motion_flag = 0;
                motion_cnt++; //motion_cnt==0时，才会对血氧进行计算，否则为原来值
                FSM_STATE = FSM_MOTION1;
            } else {
                if (motion_flag) //3s后，如果仍然检测到运动标志位，该位在ACM中设置
                {
                    FSM_STATE = FSM_MOTION2;
                    motion_cnt = 0;
                    motion_cnt++;
                    motion_flag = 0;
                    //Clear_SpO2_Num(); //把屏幕中的数据清空
                    //Clear_BPM_Num();
                    //LED_Display_Default();
                    if (SPO2orBP == 0) {
                        putstringUART1("BBB\n\r");
                        LED_Display(2, Default_SpO2, sizeof (Default_SpO2) / 128); //显示默认值
                    }

                } else //若3s后，没有检测到运动标志位
                {
                    FSM_STATE = FSM_IDLE; //进入正常模式
                    transitions[0] = -1; //重新进行血氧及脉率计算
                    transitions[1] = -1;
                    motion_cnt = 0;
                    low_cnt = 0;
                    high_cnt = 0;
                    maxIRValue = 0;
                    //minIRValue = 1.5;
                    maxRedValue = 0;
                    minIRValue = 1.5;
                    minRedValue = 1.5;
                }
            }
            break;
        case FSM_MOTION2: //又过了3s
            if (motion_cnt <= 60) //在3s内，保持不计算
            {
                motion_cnt++;
                FSM_STATE = FSM_MOTION2;
                avg_cnt = 0;
                transitions[0] = -1;
                transitions[1] = -1;
                pulseStarted = 0;
                maxIRValue = 0;
                minIRValue = 1.5;
                maxRedValue = 0;
                minRedValue = 1.5;
                Set_enable = 0;
                Reset_enable = 0;
                low_cnt = 0;
                high_cnt = 0;
            } else {
                if (motion_flag) //3s后，如果仍为运动，那么进行休眠
                {
                    invalid_finger = 0;
                    interval_counter = 0;
                    avg_cnt = 0;
                    transitions[0] = -1;
                    transitions[1] = -1;
                    pulseStarted = 0;
                    maxIRValue = 0;
                    minIRValue = 1.5;
                    maxRedValue = 0;
                    minRedValue = 1.5;
                    Set_enable = 0;
                    Reset_enable = 0;
                    Pulse_weak = 0;
                    motion_cnt = 0;
                    FSM_STATE = FSM_IDLE;
                    if (ReadI2C2Flag == 0 && SPO2orBP == 0) {
                        GotoSleep(); //进入休眠		
                        putstringUART1("CCC\n\r");
                    }
                } else //否则，重新开始计算血氧
                {
                    FSM_STATE = FSM_IDLE;
                    transitions[0] = -1;
                    transitions[1] = -1;
                    motion_cnt = 0;
                    low_cnt = 0;
                    high_cnt = 0;
                    maxIRValue = 0;
                    //minIRValue = 1.5;
                    maxRedValue = 0;
                    minIRValue = 1.5;
                    minRedValue = 1.5;
                }
            }
            break;
    }
}

/****************************************
函数名：Timer4Handler(void)描述：T4中断服务程序
主要用于定时显示波形和数据刷新
 **************************************/
void __ISR(_TIMER_4_VECTOR, ipl2) Timer4Handler(void) //中断向量 对应 中断优先级，见总手册表7.1
{
    IFS0bits.T4IF = 0; //清T4中断flag	
    LED_Display_Timer4En = 1;
}

/*
AFE数据采样中断处理程序
 */
void __ISR(_EXTERNAL_0_VECTOR, ipl5) INT0Handler(void) {

    IEC0bits.INT0IE = 0;
    IFS0bits.INT0IF = 0;
    decIR_signal = (AFE44x0_Reg_Read(0x2E) + 0x00200000) & 0x003fffff; //这些数据已经是减去背景的数据，AFE已做处理
    decRD_signal = (AFE44x0_Reg_Read(0x2F) + 0x00200000) & 0x003fffff;


    IFS0bits.INT0IF = 0;
    IEC0bits.INT0IE = 1;

	filter4(); //把采样到的数据进行4点平滑滤波，可有效去除工频干扰
    if (LP_Done == 1) //该值在filter4函数中置1
    {	
    	LP_Done = 0;
        ModelSelect(); //根据采样到的光强数据，判断是否有手指，如果有，则正常模式，如果没有，则等待模式
                //send_uart();	//把数据通过UART发送			
        while (!U1STAbits.TRMT); //确保上一次已经发送完成       
	}
}

/*
加速度传感器中断处理程序
 */
void __ISR(_EXTERNAL_4_VECTOR, ipl1) INT4Handler(void) {

    IEC0bits.INT4IE = 0;
    IFS0bits.INT4IF = 0;
    ACM_INT_Process();
    //	IFS0bits.INT4IF = 0;
    IEC0bits.INT4IE = 1;
}

/*
按钮外部中断
当按钮按下时，一方面关闭对应按钮中断，同时启动T3进行计时，500ms后，关闭T3，打开
外部按钮中断。
 */
void __ISR(_EXTERNAL_1_VECTOR, ipl6) INT1Handler(void) //外部switch中断
{

    //char temp1[20];
    //unsigned int ArrLen_tmp = 0;
    unsigned short i;
    IEC0bits.INT1IE = 0;
    IFS0bits.INT1IF = 0;
    

    TMR3 = 0;
    PR3 = 0xF9F; //1ms,一次按钮之后，500ms内不再产生中断
    T3CONSET = 0x8000; // 开始计数
    if (SleepFlag); //若此时Sleep标志位为1，说明是从休眠启动，所以只是起唤醒作用
    else //否则进行显示的切换，血氧/血压
    {
        if (SPO2orBP == 0) { //血氧
            SPO2orBP_Bef = SPO2orBP;
            //test      
            //HimmHg += 10;
            //LowmmHg += 10;
            //PR_BP += 10;
            //            
            SPO2orBP = 1;
        } else if (SPO2orBP == 1) {//血压
            SPO2orBP_Bef = SPO2orBP;
            SPO2orBP = 0;
            //SPO2orBP = 2;//test
            //SPO2orBP_mmHg_cnt = 0;
            //mmHg+=10;
            //} else if (SPO2orBP = 2) {//切换到2是调试用
            //     SPO2orBP_Bef = SPO2orBP;
            //     SPO2orBP = 0;
        }
    }
    /*
{
    LED_Display_ClearMain();
    if (SPO2orBP == 0) //血压，此处测血压时自动切换到血压显示界面，实时显示压力值，最大300mmHg,最后显示高低压和脉率
    {
        LED_Display_Default_BP(); //显示血压斜杠
        ArrLen_tmp = sizeof (BPx) / 128;
        LED_Display(0，BPx, ArrLen_tmp);
        u32tostr(HimmHg, BPH_Num, temp1);
        LED_Display_Dynamic_BP(0x18, 0x02, BPH_Num);
        u32tostr(LowmmHg, temp1, BPL_Num);
        LED_Display_Dynamic_BP_Inv(0x2C, 0x02, BPL_Num);
        u32tostr(PR_BP, Pulse_N, temp1);
        LED_Display_Dynamic_BP(0x6C, 0x02, Pulse_N);
        SPO2orBP = 1;
    } else if (SPO2orBP) //血氧
    {
        ArrLen_tmp = sizeof (SpO2x) / 128;
        LED_Display(0，SpO2x, ArrLen_tmp);
        if (Pulse_rate_LP == 0)
            Pulse_rate_OLED = 0;
        else
            Pulse_rate_OLED = Pulse_rate_LP & 0x000000ff;
        u32tostr(Pulse_rate_OLED, Pulse_Num, temp1);
        SPO2_OLED = SpO2_LP & 0x000000ff;
        u32tostr(SPO2_OLED, SpO2_Num, temp1);
        LED_Display_Dynamic(0x60, 0x02, Pulse_Num);
        LED_Display_Dynamic(0x28, 0x02, SpO2_Num);
        SPO2orBP = 0;
    }
    //SW_Interrupt();
}*/
    IFS0bits.INT1IF = 0;
    IEC0bits.INT1IE = 0; //每次中断后，不使能中断，需要定时器使能
    IFS0bits.T3IF = 0;
    IEC0bits.T3IE = 1;
}
//T3的优先级更低一点，进一次INT3后，开启计数，同时INT3不使能，
//计数到500ms之后，关闭T3，同时使能INT3。

void __ISR(_TIMER_3_VECTOR, ipl3) Timer3Handler(void) {
    IEC0bits.T3IE = 0;
    IFS0bits.T3IF = 0;

    IEC0bits.INT1IE = 0; //INT3不允许中断
    IFS0bits.INT1IF = 0;
    SwitchCnt++;
    if (SwitchCnt >= 500) //超过500ms
    {
        SwitchCnt = 0;
        T3CONbits.ON = 0;
        IFS0bits.INT1IF = 0;
        IEC0bits.INT1IE = 1; //INT3允许中断
        IFS0bits.T3IF = 0;
        IEC0bits.T3IE = 0;
    } else {
        IFS0bits.T3IF = 0;
        IEC0bits.T3IE = 1;
    }
}

/*
I2C2中断服务程序
用于与血压通讯。Comm_Process内部主要是一个状态机，用于数据发送与接收
 */
void __ISR(_I2C_2_VECTOR, ipl4) I2C2Handler(void) {
    IFS1bits.I2C2SIF = 0;
    Comm_Process();
    int_cnt++;
}

/****************************************
函数名：void main()
描述：主函数，包括初始化程序，滤波程序以及峰峰值查找和血氧值计算程序
 *****************************************/

void main() {
    unsigned int Data11[16];
    unsigned int fifo_depth = 0;
    int i = 0;
    unsigned char ACMtemp;
    BefSleepFlag = 0;
    ReadI2C2Flag = 0;
    SPO2orBP = 0;
    SPO2orBP_Bef = SPO2orBP;
    WDTCONbits.ON = 0;
    SleepFlag = 0;
    Pulse_weak = 0;
    motion_flag = 0;
    initPORTs(); //对IO管脚进行配置
    initUART1(8); //配置波特率  115200
    //initRTCC(InitDate, InitTime);//不用该功能

    initSPI2_Master();
    initSPI1_Master();
    IEC1bits.SPI1RXIE = 0;
    IFS1bits.SPI1RXIF = 0;
    AFE_Interrupt_init();
    AFE_init();
    ACMint_init();
    initI2C1();
    initACM();


    INTCONbits.MVEC = 1; //采用多向量模式
    INTEnableInterrupts(); //使能全局中断，CPU寄存器中由status寄存器控制 

    Init_OLED();
    initTimer4();
    initTimer3();
    SW_Interrupt_init();
    //initI2C2();

    //putstringUART1("Init_OK!");


    while (1) {	

        if (BefSleepFlag == 0 && LED_Display_Timer4En == 1) {
            LED_Display_Timer4En = 0;
            LED_Display_Change();
        }
		GotoIdle(); //每次采样周期结束后，进入IDLE模式	
    }
}

/****************************************
函数名：filter4()
描述：4点平滑滤波程序，将BG/IR/RD转换为光强数据，并做滤波，同时还包括脉率信号和血氧信号
 **************************************/
void filter4() {
    //desampling
    RD_Intensity = RD_Intensity + decRD_signal;
    IR_Intensity = IR_Intensity + decIR_signal;
    Desampling = Desampling + 1;

    if (Desampling >= Desampling_rate) //四次采集后才进入
    {
        Desampling = 0;
        //LP pass
        RD_LP = (RD_temp1 + RD_temp2 + RD_temp3 + RD_Intensity) >> 4; //光滑滤波
        RD_temp3 = RD_temp2;
        RD_temp2 = RD_temp1;
        RD_temp1 = RD_Intensity;
        f_RD = ((float) RD_LP) / 4194303 * 2.4 - 1.2; //转换为对应电压

        IR_LP = (IR_temp1 + IR_temp2 + IR_temp3 + IR_Intensity) >> 4;
        IR_temp3 = IR_temp2;
        IR_temp2 = IR_temp1;
        IR_temp1 = IR_Intensity;
        f_IR = ((float) IR_LP) / 4194303 * 2.4 - 1.2;
        RD_Intensity = 0;
        IR_Intensity = 0;

        LP_Done = 1;
        abc_test = RD_LP; //用于数据打印
        abc_test_dc = IR_LP; //用于数据打印

    }
}

/****************************************
函数名：ModelSelect()
描述：将光强信号（主要是RD）进行比较，若不在合理范围内，则视为没有手指，则切换模式。
      在等待模式下超过10s，系统进行SLEEP模式。同时在切换过程中对BASE进行控制，降低功耗
 **************************************/








void ModelSelect() {
    if ((f_IR >= Hz_upper_threshold) || (f_IR <= Hz_lower_threshold)) {
        Bef_GotoSleep(); //若光强在设定范围外，就认为异常，一定时间后就休眠	
    } else {
        if (BefSleepFlag == 1)
            LED_Fill(0);
        BefSleepFlag = 0; //否则就进行正常的数据处理
        invalid_finger = 0;
        Pulse_rate_fn(); //数据处理函数
    }
}

/*
休眠前的处理函数
 */
void Bef_GotoSleep() {
    invalid_finger = invalid_finger + 1;
    if (invalid_finger < 250) //一定时间以内，清零之前计算的数据
    {
        transitions[0] = -1;
        transitions[1] = -1;
        pulseStarted = 0;
        maxIRValue = 0;
        minIRValue = 1.5;
        maxRedValue = 0;
        minRedValue = 1.5;
        Set_enable = 0;
        Reset_enable = 0;
        Pulse_weak = 0;
        low_cnt = 0;
        high_cnt = 0;
    }
    if (invalid_finger >= no_finger_threshold) //超过时间阈值
    { //且未在发数状态机内
        if (ReadI2C2Flag == 0 && SPO2orBP != 2)//在测血压时不休眠
            GotoSleep(); //进入休眠	
    } else if (invalid_finger >= (no_finger_threshold - 250)) {
        BefSleepFlag = 1;
        LED_Display(2, No_Finger, sizeof (No_Finger) / 128);
        interval_counter = 0;
        avg_cnt = 0;
    }
}

/*
滤波函数
首先将输入的波形，进行高通滤波，去掉基线
之后，进行16个数据的平滑滤波，达到低通的效果，频率大概在3Hz左右
 */
unsigned int FilterHP(int sample) {
    HB_counter++;
    if (HB_counter > HP_f) //高通滤波，去基线
        HB_counter = 0;
    Pulse_HP = sample + Const3; //const3防止有负数的情况
    Pulse_HP = Pulse_HP - (Pulse_HP_templ >> HP_f_bit); //÷32是数组里面32个数做个平均。
    Pulse_HP_templ = Pulse_HP_templ - Pulse_HP_buffer[HB_counter] + sample; //这个的思路很巧妙  (但会有溢出的可能)
    Pulse_HP_buffer[HB_counter] = sample; //32个数里面减去最老的那个数，加上最新的数。

    fil_counter++; //用上述方法实现16个数的平滑滤波（3Hz），得到Pulse_HP_Total_avg
    if (fil_counter > 15) //该信号用于峰峰值判断
        fil_counter = 0;
    Pulse_HP_Total_avg = Pulse_HP_Total >> 4; //均值滤波（低通滤波）
    Pulse_HP_Total = Pulse_HP_Total - Pulse_HP_temp[fil_counter] + Pulse_HP;
    Pulse_HP_temp[fil_counter] = Pulse_HP;

    Pulse = Pulse_HP_Total_avg;
    return Pulse;
}

/*
血氧计算函数
参数：红外最大值、红外最小值、红光最大值、红光最小值
 */
unsigned int calcPulseOx(float maxIR, float minIR, float maxRed, float minRed) {
    float irDC = minIR;
    float irAC = maxIR - minIR;
    float redDC = minRed;
    float redAC = maxRed - minRed;
    unsigned int tSpO2 = 0;
    //abc_test = maxRed;   //用于数据打印
    //abc_test_dc = minRed; //用于数据打印

    //去掉AC、DC负数或是零的情况
    if (redAC <= 0.00006)
        redAC = redACtmp;
    if (redDC == 0.02)
        redDC = redDCtemp;
    if (irAC <= 0.00006)
        irAC = irACtmp;
    if (irDC == 0.02)
        irDC = irDCtemp;
    redACtmp = redAC;
    redDCtemp = redDC;
    irACtmp = irAC;
    irDCtemp = irDC;

    float ratio = (redAC / redDC) / (irAC / irDC);
    //putstringUART1("\r\n");
    //SendDecimalismDta(ratio*1000);
    //tSpO2 = -12.39*ratio*ratio-18.47*ratio+111.9;  //7.29N-20,20p,RED+IR,SENSOR:#059
    tSpO2 = 120.7 - 40.04 * ratio; //7.29 N-20,20p,RED+IR,SENSOR:#059
    return tSpO2;
}

/*
脉率计算函数
参数：两次峰峰值的计数值
 */
unsigned int calcHeartRate(int start, int end) {
    unsigned int pulseLength = 0;

    if (start > end) end += 300; //In case end index of pulse wrapped around 6 -second window
    pulseLength = end - start; //Calculate length of pulse based on start and end indices
    //Check if this is reasonable pulse length
    if ((pulseLength >= sampleRate / 4) && (pulseLength < sampleRate * 3)) {
        unsigned int heartRate = (60.00 * sampleRate) / pulseLength;
        return heartRate;
    }

    return 0; //Return 0 for invalid pulse length
}

/****************************************
函数名：Pulse_rate_fn
描述：脉率计算和SpO2计算函数，同时包括高通滤波和峰峰值查询。若峰峰值查询超时则视为低灌注
      如果超过时间过长，则视为不是手指，系统进入SLEEP模式
 **************************************/
unsigned int Pulse_rate_fn() {
    interval_counter++;

    filtIRSample = FilterHP(IR_LP + RD_LP);
    //Start calculating data after skipping 2 seconds
    if (interval_counter > 100) //2s后相对稳定些再计算
    {
        unsigned int index = interval_counter - 100;

        //Find max and min IR and Red values in the current pulse
        Pulse_buffer_counter = Pulse_buffer_counter + 1;
        if (Pulse_buffer_counter > (Pulse_buffer_size - 1)) {
            Pulse_buffer_counter = 0;
        }
        IR_buffer[Pulse_buffer_counter] = f_IR; //把对应的电压数据和
        RD_buffer[Pulse_buffer_counter] = f_RD; //滤波后数据放入数组
        HP_buffer[Pulse_buffer_counter] = Pulse_HP; //用于后续的血氧和脉率计算
        if (pulseStarted) {
            if (f_RD > maxRedValue) maxRedValue = f_RD;
            if (f_RD < minRedValue) minRedValue = f_RD;
            if (f_IR > maxIRValue) //找到最大最小值，同时记录下他们对应的索引
            {
                maxIRValue = f_IR;
                ind_max = Pulse_buffer_counter;
            }
            if (f_IR < minIRValue) {
                minIRValue = f_IR;
                ind_min = Pulse_buffer_counter;
            }
        }
        //这部分的主要作用为波形显示，一方面将采样的数据用一个较大的常数1.5*const
        //去减，不然波形是反的，另外，为了去除漂移的影响，得到当前值，以及之前最小值和
        //最大值，用于波形归一化显示。最大最小值都是从每个周期找出来的，在不断更新，只是
        //是上一个周期的
        if (Pulse_HP_MIN != 0) {
            f_HP = (float) (1.5 * Const3 - Pulse_HP) / (float) (1.5 * Const3 - Pulse_HP_MIN);
            f_HP_min = (float) (1.5 * Const3 - Pulse_HP_MAX) / (float) (1.5 * Const3 - Pulse_HP_MIN);
        } else
            f_HP = 0;

        ppreviousValue = previousValue; //3个数据，用于峰值的计算
        previousValue = currentValue;
        currentValue = filtIRSample; //为了避免噪声对峰值估计的影响
        low_cnt++; //用3Hz的波形进行峰值寻找，同时
        high_cnt++; //用实际波形的最大最小值进行血氧计算

        //找峰值(为什么每次找到最小值就可以进行SPO2等值的计算了，因为类似于正弦波的波形，最大值比最小值先出现，如果最小值找到了，那这个周期的峰峰值寻找完毕)
        if ((previousValue <= ppreviousValue) && (previousValue < currentValue)&& (!Set_enable) && (high_cnt >= 3))//&& (currentValue <=Const3))  //检测最小值
        {
            low_cnt = 0;
            Set_enable = 1;
            Reset_enable = 0;
            if (transitions[0] == -1) transitions[0] = index; //第一个峰值
            else if (transitions[1] == -1) //第二个峰值
            {
                transitions[1] = index;
                pulseStarted = 1; //对应脉率检测标志位置位
            } else {
                transitions[0] = transitions[1];
                transitions[1] = index;
                Pulse_HP_MAX = HP_buffer[ind_max]; //得到最大值

                //Call signal functions to calculate heart rate and SpO2
                if ((motion_flag != 1) && (motion_cnt == 0)) //没在剧烈运动
                {
                    Pulse_rate = calcHeartRate(transitions[0], transitions[1]);
                    SPO2 = calcPulseOx(IR_buffer[ind_max], IR_buffer[ind_min], RD_buffer[ind_max], RD_buffer[ind_min]);
                    Pulse_amp = IR_buffer[ind_max] - IR_buffer[ind_min];
                    //低灌注
                    if (Pulse_amp <= Amp_threshold)
                        Pulse_weak++;
                    else
                        Pulse_weak = 0;
                    if (Pulse_weak >= 8) { //此处增加判断是否在测血压，只在血氧时休眠                     
                        if (ReadI2C2Flag == 0 && SPO2orBP == 0)
                            GotoSleep(); //进入休眠	
                    }
                    if (SPO2 >= 100)
                        SPO2 = SPO2_pre;
                    SPO2_pre = SPO2;
                    avg_fun();
                    /*Pulse_rate_LP=(Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate_temp3 + Pulse_rate)>>2;
                    Pulse_rate_temp3 = Pulse_rate_temp2;
                    Pulse_rate_temp2 = Pulse_rate_temp1;
                    Pulse_rate_temp1 = Pulse_rate;
                    SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;
                    SpO2_temp3 = SpO2_temp2;
                    SpO2_temp2 = SpO2_temp1;
                    SpO2_temp1 = SPO2;*/
                }
                //Reset maximum and minimum defaults for comparison in next cycle
                maxIRValue = 0; //检测到最大值后，同时把最大值清空
                //minIRValue = 1.5;			//为下一次最大值检测做准备
                maxRedValue = 0;
                //minRedValue = 1.5;
            }
        }//最大值检测
        else if ((previousValue >= ppreviousValue) && (previousValue > currentValue)&&(!Reset_enable) && (low_cnt >= 3))// && (currentValue >= Const3))
        {
            high_cnt = 0;
            Set_enable = 0;
            Reset_enable = 1;
            minIRValue = 1.5; //最小值复位，为下一次检测准备，不然出现一次异常情况，比如最小值飙到了-0.1，那之后就再也没法取到正常值的最小值了
            minRedValue = 1.5;
            Pulse_HP_MIN = HP_buffer[ind_min]; //得到最小值
        }
    }
    if (interval_counter >= 400) {
        interval_counter = 100;
    }
}

/*
起始数据平均函数
在最开始，因为考虑到所有数据是4个数平均，所以在最开始进行了特殊的处理
以确保一开始的数据偏差不会太大
 */
void avg_fun() {
    avg_cnt++;
    if (avg_cnt == 1) //采样到第一个数据时，其他3个也为该数据
    {
        Pulse_rate_LP = Pulse_rate; //潘
        Pulse_rate_temp1 = Pulse_rate;

        SpO2_LP = SPO2;
        SpO2_temp1 = SPO2;

    } else if (avg_cnt == 2) //采样到第二个数据时，其他3个数据也为该数据
    {

        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate) >> 1; //潘
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;

        SpO2_LP = (SpO2_temp1 + SPO2) >> 1; //pan
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;

    }//weibin
        /*else if(avg_cnt == 3)			//采样到第三个数据时，最后一个数位最新的这个数据
        {
            Pulse_rate_temp3 = Pulse_rate;
            Pulse_rate_temp2 = Pulse_rate_temp1;
            Pulse_rate_temp1 = Pulse_rate;
            SpO2_temp3 = SPO2;
            SpO2_temp2 = SpO2_temp1;
            SpO2_temp1 = SPO2;	
        }*/
        //pan
    else if (avg_cnt == 3) //采样到第3个数据时，最后一个数位最新的这个数据
    {
        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate) / 3; //潘
        Pulse_rate_temp3 = Pulse_rate_temp2;
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;
        //SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;  //weibin
        SpO2_LP = (SpO2_temp1 + SpO2_temp2 + SPO2) / 3; //pan
        SpO2_temp3 = SpO2_temp2;
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;
    }//pan
    else if (avg_cnt == 4) //采样到第4个数据时，最后一个数位最新的这个数据
    {
        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate_temp3 + Pulse_rate) >> 2; //潘
        Pulse_rate_temp4 = Pulse_rate_temp3;
        Pulse_rate_temp3 = Pulse_rate_temp2;
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;
        //SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;  //weibin
        SpO2_LP = (SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2) >> 2; //pan
        SpO2_temp4 = SpO2_temp3;
        SpO2_temp3 = SpO2_temp2;
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;
    }//pan
    else if (avg_cnt == 5) //采样到第5个数据时，最后一个数位最新的这个数据
    {
        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate_temp3 + Pulse_rate_temp4 + Pulse_rate) / 5; //潘
        Pulse_rate_temp5 = Pulse_rate_temp4;
        Pulse_rate_temp4 = Pulse_rate_temp3;
        Pulse_rate_temp3 = Pulse_rate_temp2;
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;
        //SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;  //weibin
        SpO2_LP = (SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SpO2_temp4 + SPO2) / 5; //pan
        SpO2_temp5 = SpO2_temp4;
        SpO2_temp4 = SpO2_temp3;
        SpO2_temp3 = SpO2_temp2;
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;
    }//pan
    else if (avg_cnt == 6) //采样到第6个数据时，最后一个数位最新的这个数据
    {
        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate_temp3 + Pulse_rate_temp4 + Pulse_rate_temp5 + Pulse_rate) / 6; //潘
        Pulse_rate_temp6 = Pulse_rate_temp5;
        Pulse_rate_temp5 = Pulse_rate_temp4;
        Pulse_rate_temp4 = Pulse_rate_temp3;
        Pulse_rate_temp3 = Pulse_rate_temp2;
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;
        //SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;  //weibin
        SpO2_LP = (SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SpO2_temp4 + SpO2_temp5 + SPO2) / 6; //pan
        SpO2_temp6 = SpO2_temp5;
        SpO2_temp5 = SpO2_temp4;
        SpO2_temp4 = SpO2_temp3;
        SpO2_temp3 = SpO2_temp2;
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;
    }//pan
    else if (avg_cnt == 7) //采样到第7个数据时，最后一个数位最新的这个数据
    {
        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate_temp3 + Pulse_rate_temp4 + Pulse_rate_temp5 + Pulse_rate_temp6 + Pulse_rate) / 7; //潘
        Pulse_rate_temp7 = Pulse_rate_temp6;
        Pulse_rate_temp6 = Pulse_rate_temp5;
        Pulse_rate_temp5 = Pulse_rate_temp4;
        Pulse_rate_temp4 = Pulse_rate_temp3;
        Pulse_rate_temp3 = Pulse_rate_temp2;
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;
        //SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;  //weibin
        SpO2_LP = (SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SpO2_temp4 + SpO2_temp5 + SpO2_temp6 + SPO2) / 7; //pan
        SpO2_temp7 = SpO2_temp6;
        SpO2_temp6 = SpO2_temp5;
        SpO2_temp5 = SpO2_temp4;
        SpO2_temp4 = SpO2_temp3;
        SpO2_temp3 = SpO2_temp2;
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;
    }//weibin
        /*else if(avg_cnt>=4)
            avg_cnt = 4;
         */
        //pan
    else if (avg_cnt >= 8) {
        avg_cnt = 8;
        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate_temp3 + Pulse_rate_temp4 + Pulse_rate_temp5 + Pulse_rate_temp6 + Pulse_rate_temp7 + Pulse_rate) >> 3; //潘
        Pulse_rate_temp7 = Pulse_rate_temp6;
        Pulse_rate_temp6 = Pulse_rate_temp5;
        Pulse_rate_temp5 = Pulse_rate_temp4;
        Pulse_rate_temp4 = Pulse_rate_temp3;
        Pulse_rate_temp3 = Pulse_rate_temp2;
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;
        //SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;  //weibin
        SpO2_LP = (SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SpO2_temp4 + SpO2_temp5 + SpO2_temp6 + SpO2_temp7 + SPO2) >> 3; //pan
        SpO2_temp7 = SpO2_temp6;
        SpO2_temp6 = SpO2_temp5;
        SpO2_temp5 = SpO2_temp4;
        SpO2_temp4 = SpO2_temp3;
        SpO2_temp3 = SpO2_temp2;
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;
    }
    //SendDecimalismDta(SPO2);
}

/**
 * 对血氧和脉率的显示值进行滤波处理
 */
void SpO2_Pulse_Display_avg_fun(void) {
    SportFSM();
    WaveCnt++;
    if (WaveCnt >= 20) //波形1/20s更新一次，数值1s更新一次。
    {
        WaveCnt = 0;
        if (SPO2orBP == 0) {
            if (avg_cnt != 0) {
                if (Pulse_rate_LP == 0)
                    Pulse_rate_OLED = 0;
                else
                    Pulse_rate_OLED = Pulse_rate_LP & 0x000000ff;
                SPO2_OLED = SpO2_LP & 0x000000ff;
                if (OLEDSPO2_cnt == 0) {
                    SPO2_OLED_display = SPO2_OLED;
                    SPO2_OLED_temp1 = SPO2_OLED;

                    Pulse_rate_OLED_display = Pulse_rate_OLED;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt == 1) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED) >> 1;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED;

                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED) >> 1;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt == 2) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED) / 3;
                    SPO2_OLED_temp3 = SPO2_OLED_temp2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED;

                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED_temp2 + Pulse_rate_OLED) / 3;
                    Pulse_rate_OLED_temp3 = Pulse_rate_OLED_temp2;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt == 3) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED) >> 2;
                    SPO2_OLED_temp4 = SPO2_OLED_temp3;
                    SPO2_OLED_temp3 = SPO2_OLED_temp2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED;

                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED_temp2 + Pulse_rate_OLED_temp3 + Pulse_rate_OLED) >> 2;
                    Pulse_rate_OLED_temp4 = Pulse_rate_OLED_temp3;
                    Pulse_rate_OLED_temp3 = Pulse_rate_OLED_temp2;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt == 4) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED_temp4 + SPO2_OLED) / 5;
                    SPO2_OLED_temp5 = SPO2_OLED_temp4;
                    SPO2_OLED_temp4 = SPO2_OLED_temp3;
                    SPO2_OLED_temp3 = SPO2_OLED_temp2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED;


                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED_temp2 + Pulse_rate_OLED_temp3 + Pulse_rate_OLED_temp4 + Pulse_rate_OLED) / 5;
                    Pulse_rate_OLED_temp5 = Pulse_rate_OLED_temp4;
                    Pulse_rate_OLED_temp4 = Pulse_rate_OLED_temp3;
                    Pulse_rate_OLED_temp3 = Pulse_rate_OLED_temp2;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt == 5) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED_temp4 + SPO2_OLED_temp5 + SPO2_OLED) / 6;
                    SPO2_OLED_temp6 = SPO2_OLED_temp5;
                    SPO2_OLED_temp5 = SPO2_OLED_temp4;
                    SPO2_OLED_temp4 = SPO2_OLED_temp3;
                    SPO2_OLED_temp3 = SPO2_OLED_temp2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED;

                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED_temp2 + Pulse_rate_OLED_temp3 + Pulse_rate_OLED_temp4 + Pulse_rate_OLED_temp5 + Pulse_rate_OLED) / 6;
                    Pulse_rate_OLED_temp6 = Pulse_rate_OLED_temp5;
                    Pulse_rate_OLED_temp5 = Pulse_rate_OLED_temp4;
                    Pulse_rate_OLED_temp4 = Pulse_rate_OLED_temp3;
                    Pulse_rate_OLED_temp3 = Pulse_rate_OLED_temp2;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt == 6) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED_temp4 + SPO2_OLED_temp5 + SPO2_OLED_temp6 + SPO2_OLED) / 7;
                    SPO2_OLED_temp7 = SPO2_OLED_temp6;
                    SPO2_OLED_temp6 = SPO2_OLED_temp5;
                    SPO2_OLED_temp5 = SPO2_OLED_temp4;
                    SPO2_OLED_temp4 = SPO2_OLED_temp3;
                    SPO2_OLED_temp3 = SPO2_OLED_temp2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED;

                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED_temp2 + Pulse_rate_OLED_temp3 + Pulse_rate_OLED_temp4 + Pulse_rate_OLED_temp5 + Pulse_rate_OLED_temp6 + Pulse_rate_OLED) / 7;
                    Pulse_rate_OLED_temp7 = Pulse_rate_OLED_temp6;
                    Pulse_rate_OLED_temp6 = Pulse_rate_OLED_temp5;
                    Pulse_rate_OLED_temp5 = Pulse_rate_OLED_temp4;
                    Pulse_rate_OLED_temp4 = Pulse_rate_OLED_temp3;
                    Pulse_rate_OLED_temp3 = Pulse_rate_OLED_temp2;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt >= 7) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED_temp4 + SPO2_OLED_temp5 + SPO2_OLED_temp6 + SPO2_OLED_temp7 + SPO2_OLED) >> 3;
                    SPO2_OLED_temp7 = SPO2_OLED_temp6;
                    SPO2_OLED_temp6 = SPO2_OLED_temp5;
                    SPO2_OLED_temp5 = SPO2_OLED_temp4;
                    SPO2_OLED_temp4 = SPO2_OLED_temp4;
                    SPO2_OLED_temp3 = SPO2_OLED_temp2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED_display;

                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED_temp2 + Pulse_rate_OLED_temp3 + Pulse_rate_OLED_temp4 + Pulse_rate_OLED_temp5 + Pulse_rate_OLED_temp6 + Pulse_rate_OLED_temp7 + Pulse_rate_OLED) >> 3;
                    Pulse_rate_OLED_temp7 = Pulse_rate_OLED_temp6;
                    Pulse_rate_OLED_temp6 = Pulse_rate_OLED_temp5;
                    Pulse_rate_OLED_temp5 = Pulse_rate_OLED_temp4;
                    Pulse_rate_OLED_temp4 = Pulse_rate_OLED_temp3;
                    Pulse_rate_OLED_temp3 = Pulse_rate_OLED_temp2;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED_display;
                    OLEDSPO2_cnt = 7;
                }
            }
        }
    }
}

/****************************************
函数名：send_uart()
描述：UART发送函数，可将对应的数据通过UART发送，99和88为标志位
 **************************************/
void send_uart(void) {
    /*putcharUART1(99);
    putcharUART1(88);
    p_RD = (unsigned char*)(&abc_test);
    p_IR = (unsigned char*)(&abc_test_dc);*/



    /*	putcharUART1(Pulse_HP);
        putcharUART1(Pulse_HP>>8);
        putcharUART1(Pulse_HP>>16);
        putcharUART1(Pulse_HP>>24);
     */
    /*putcharUART1(*p_RD);
    putcharUART1(*(p_RD+1));
    putcharUART1(*(p_RD+2));
    putcharUART1(*(p_RD+3));

    putcharUART1(*p_IR);
    putcharUART1(*(p_IR+1));
    putcharUART1(*(p_IR+2));
    putcharUART1(*(p_IR+3));

    putcharUART1(Pulse_HP_Total_avg);
    putcharUART1(Pulse_HP_Total_avg>>8);
    putcharUART1(Pulse_HP_Total_avg>>16);
    putcharUART1(Pulse_HP_Total_avg>>24);*/

    /*
        putcharUART1(SPO2);
        putcharUART1(SPO2>>8);

        putcharUART1(Pulse_rate);
        putcharUART1(Pulse_rate>>8);
        putcharUART1(Pulse_rate>>16);
        putcharUART1(Pulse_rate>>24);

        putcharUART1(currentValue);
        putcharUART1(currentValue>>8);
        putcharUART1(currentValue>>16);
        putcharUART1(currentValue>>24);
     */

    //putcharUART1(SPO2);
    //putcharUART1(SPO2>>8);
}
