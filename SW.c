/* 
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 作者              日期      
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 魏彬       		2014年12月26日	
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 描述
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
按钮的初始化和处理函数
 **********************************************************************/

#include "p32mx150f128b.h"
#include "stdio.h"
#include "extern_func.h"
#include "Port_init.h"
#include <plib.h>

unsigned int test_data = 0;

//按钮中断初始化

void SW_Interrupt_init() {
    IEC0bits.INT1IE = 0;
    IFS0bits.INT1IF = 0;
    IPC1bits.INT1IP = 6;
    IPC1bits.INT1IS = 3;
    INTCONbits.INT1EP = 0; //下降沿触发
    IFS0bits.INT1IF = 0;
    IEC0bits.INT1IE = 1;
}

//按钮中断处理函数，血压显示

void SW_Interrupt() {
    char temp1[20];
    unsigned int ArrLen_tmp = 0;
    unsigned char cnt = 0;
    //	LED_Display_Default_BP();
    //	ArrLen_tmp = sizeof(BPx)/128;
    //	LED_Display(0,BPx,ArrLen_tmp);             //此处增加血压转屏显示功能，高低压用竖杠隔离
    u32tostr(HimmHg, BPH_Num, temp1);
    LED_Display_Dynamic_BP(0x18, 0x02, BPH_Num);
    u32tostr(LowmmHg, temp1, BPL_Num);
    LED_Display_Dynamic_BP_Inv(0x2C, 0x02, BPL_Num);
    u32tostr(PR_BP, Pulse_N, temp1);
    LED_Display_Dynamic_BP(0x6C, 0x02, Pulse_N);

}