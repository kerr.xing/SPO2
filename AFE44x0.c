#include "AFE44x0.h"
/**************************************************************************************************************************************************
*	        Prototypes									                                                  										  *
**************************************************************************************************************************************************/

/**************************************************************************************************************************************************
*	        Global Variables										                                  											  *
**************************************************************************************************************************************************/
#define DELAY_COUNT 2

unsigned int AFE44xx_Current_Register_Settings[36] = {
  CONTROL0_VAL,           //Reg0:CONTROL0: CONTROL REGISTER 0
  LED2STC_VAL,            //Reg1:REDSTARTCOUNT: SAMPLE RED START COUNT
  LED2ENDC_VAL,           //Reg2:REDENDCOUNT: SAMPLE RED END COUNT
  LED2LEDSTC_VAL,         //Reg3:REDLEDSTARTCOUNT: RED LED START COUNT
  LED2LEDENDC_VAL,        //Reg4:REDLEDENDCOUNT: RED LED END COUNT
  ALED2STC_VAL,           //Reg5:AMBREDSTARTCOUNT: SAMPLE AMBIENT RED START COUNT
  ALED2ENDC_VAL,          //Reg6:AMBREDENDCOUNT: SAMPLE AMBIENT RED END COUNT
  LED1STC_VAL,            //Reg7:IRSTARTCOUNT: SAMPLE IR START COUNT
  LED1ENDC_VAL,           //Reg8:IRENDCOUNT: SAMPLE IR END COUNT
  LED1LEDSTC_VAL,         //Reg9:IRLEDSTARTCOUNT: IR LED START COUNT
  LED1LEDENDC_VAL,        //Reg10:IRLEDENDCOUNT: IR LED END COUNT
  ALED1STC_VAL,           //Reg11:AMBIRSTARTCOUNT: SAMPLE AMBIENT IR START COUNT
  ALED1ENDC_VAL,          //Reg12:AMBIRENDCOUNT: SAMPLE AMBIENT IR END COUNT
  LED2CONVST_VAL,         //Reg13:REDCONVSTART: REDCONVST
  LED2CONVEND_VAL,        //Reg14:REDCONVEND: RED CONVERT END COUNT
  ALED2CONVST_VAL,        //Reg15:AMBREDCONVSTART: RED AMBIENT CONVERT START COUNT
  ALED2CONVEND_VAL,       //Reg16:AMBREDCONVEND: RED AMBIENT CONVERT END COUNT
  LED1CONVST_VAL,         //Reg17:IRCONVSTART: IR CONVERT START COUNT
  LED1CONVEND_VAL,        //Reg18:IRCONVEND: IR CONVERT END COUNT
  ALED1CONVST_VAL,        //Reg19:AMBIRCONVSTART: IR AMBIENT CONVERT START COUNT
  ALED1CONVEND_VAL,       //Reg20:AMBIRCONVEND: IR AMBIENT CONVERT END COUNT
  ADCRSTSTCT0_VAL,        //Reg21:ADCRESETSTCOUNT0: ADC RESET 0 START COUNT
  ADCRSTENDCT0_VAL,       //Reg22:ADCRESETENDCOUNT0: ADC RESET 0 END COUNT
  ADCRSTSTCT1_VAL,        //Reg23:ADCRESETSTCOUNT1: ADC RESET 1 START COUNT
  ADCRSTENDCT1_VAL,       //Reg24:ADCRESETENDCOUNT1: ADC RESET 1 END COUNT
  ADCRSTSTCT2_VAL,        //Reg25:ADCRESETENDCOUNT2: ADC RESET 2 START COUNT
  ADCRSTENDCT2_VAL,       //Reg26:ADCRESETENDCOUNT2: ADC RESET 2 END COUNT
  ADCRSTSTCT3_VAL,        //Reg27:ADCRESETENDCOUNT3: ADC RESET 3 START COUNT
  ADCRSTENDCT3_VAL,       //Reg28:ADCRESETENDCOUNT3: ADC RESET 3 END COUNT
  PRP,                    //Reg29:PRPCOUNT: PULSE REPETITION PERIOD COUNT
  CONTROL1_VAL,           //Reg30:CONTROL1: CONTROL REGISTER 1 //timer enabled, averages=3, RED and IR LED pulse ON PD_ALM AND LED_ALM pins
  0x00000,                //Reg31:?: ??          
  (ENSEPGAIN + STAGE2EN_LED1 + STG2GAIN_LED1_0DB + CF_LED1_5P + RF_LED1_1M),                      //Reg32:TIAGAIN: TRANS IMPEDANCE AMPLIFIER GAIN SETTING REGISTER
  (AMBDAC_0uA + FLTRCNRSEL_500HZ + STAGE2EN_LED2 + STG2GAIN_LED2_0DB + CF_LED2_5P + RF_LED2_1M),  //Reg33:TIA_AMB_GAIN: TRANS IMPEDANCE AAMPLIFIER AND AMBIENT CANELLATION STAGE GAIN
  (LEDCNTRL_VAL),                                                                                 //Reg34:LEDCNTRL: LED CONTROL REGISTER
  (TX_REF_1 + RST_CLK_ON_PD_ALM_PIN_DISABLE + ADC_BYP_DISABLE + TXBRGMOD_H_BRIDGE + DIGOUT_TRISTATE_DISABLE + XTAL_ENABLE + EN_FAST_DIAG + PDN_TX_OFF + PDN_RX_OFF + PDN_AFE_OFF)                 //Reg35:CONTROL2: CONTROL REGISTER 2 //bit 9
};

/**********************************************************************************************************
* Init_AFE44xx_DRDY_Interrupt												                                          *
**********************************************************************************************************/
void Init_AFE44xx_DRDY_Interrupt (void)
{
  P2DIR &= ~AFE_ADC_DRDY;
  P2REN |= AFE_ADC_DRDY;                              // Enable P2.3 internal resistance
  P2OUT |= AFE_ADC_DRDY;                            	// Set P2.3 as pull-Up resistance
  P2IES |= AFE_ADC_DRDY;                              // P2.3 Hi/Lo edge
  P2IFG &= ~AFE_ADC_DRDY;                           	// P2.3 IFG cleared
  P2IE &= ~AFE_ADC_DRDY;                             	// P2.3 interrupt disabled
}

/**********************************************************************************************************
* Enable_AFE44xx_DRDY_Interrupt												                                          *
**********************************************************************************************************/
void Enable_AFE44xx_DRDY_Interrupt (void)
{
  P2IFG &= ~AFE_ADC_DRDY;                           	// P2.3 IFG cleared
  P2IE |= AFE_ADC_DRDY;                             	// P2.3 interrupt enabled
}

/**********************************************************************************************************
* Disable_AFE44xx_DRDY_Interrupt												                                          *
**********************************************************************************************************/
void Disable_AFE44xx_DRDY_Interrupt (void)
{
  P2IFG &= ~AFE_ADC_DRDY;                           	// P2.3 IFG cleared
  P2IE &= ~AFE_ADC_DRDY;                             	// P2.3 interrupt disabled
}

/**********************************************************************************************************
* Set_GPIO														                                          *
**********************************************************************************************************/
void Set_GPIO(void)
{
  //port set..
  //Port 1.1 - AFE_RESETZ, P1.2 - AFE_PDNZ, P2.3 - ADC_RDY, P2.4- PD_ALM, P2.5 - LED_ALM,
  //P5.7 - DIAG_END
  
  P1SEL |= BIT0;
  P1DIR |= BIT0;
  P1OUT |= BIT0;
  
  P1DIR |= (AFE_RESETZ + AFE_PDNZ);
  P1OUT |= (AFE_RESETZ + AFE_PDNZ);
  P2DIR &= ~(AFE_ADC_DRDY + AFE_PD_ALM + AFE_LED_ALM);
  P5DIR &= ~AFE_DIAG_END;
  
  P2SEL = 0x00;
  P2DIR &= BIT0;
  P2OUT |= (BIT1 + BIT2 + BIT7);
  P2DIR |= (BIT1 | BIT2 | BIT7);
}

/**********************************************************************************************************
* Init_AFE44xx_Resource						                                          *
**********************************************************************************************************/

void Init_AFE44xx_Resource(void)
{
  Set_GPIO();										// Initializes AFE44xx's input control lines
}

/**********************************************************************************************************
*	        AFE44xx_Read_All_Regs          				                  					  *
**********************************************************************************************************/

void AFE44xx_Read_All_Regs(unsigned long AFE44xxeg_buf[])
{
  unsigned char Regs_i;
  for ( Regs_i = 0; Regs_i < 50; Regs_i++)
  {
    AFE44xxeg_buf[Regs_i] = AFE44xx_Reg_Read(Regs_i);
  }
}
/*********************************************************************************************************/
/**********************************************************************************************************
*	        AFE44xx_PowerOn_Init          				                  					  			  *
***********************************************************************************************************/
void AFE44xx_PowerOn_Init(void)
{
  volatile unsigned short Init_i, j;
  Init_AFE44xx_Resource();
  for (j = 0; j < DELAY_COUNT; j++)
  {
    for ( Init_i =0; Init_i < 20000; Init_i++);
    for ( Init_i =0; Init_i < 20000; Init_i++);
    for ( Init_i =0; Init_i < 20000; Init_i++);
  }
  Init_AFE44xx_DRDY_Interrupt();
  AFE44xx_Default_Reg_Init();
}