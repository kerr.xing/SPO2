/* 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 作者              日期      
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 魏彬       		2014年7月30日	
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 描述
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 根据第四版电路图，对UART1、LED的RED/IR控制以及LED的电流大小切换的引脚进行定义。
*****************************************************************************************************************/

#ifndef __PORTINIT_H
#define __PORTINIT_H

//*****************define I/O control registers*************//
//************AFE*************//
#define RST_AFE_TRIS _TRISB6 	 	//OUTPUT
#define RST_AFE _LATB6
#define STE_TRIS _TRISB10 		//OUTPUT
#define STE _LATB10
#define SIMO_TRIS _TRISB11 //_TRISC9		//OUTPUT	
#define SIMO _LATB11 	//_LATC9			//
#define ADC_RDY_TRIS _TRISB7 //_TRISC8		//OUTPUT	
#define ADC_RDY _LATB7 	//_LATC8			//
#define SOMI_TRIS _TRISB5		//OUTPUT
#define SOMI _LATB5
#define PWD_AFE_TRIS _TRISB12		//OUTPUT
#define PWD_AFE _LATB12
#define SPI_CLK_TRIS _TRISB14		//OUTPUT
#define SPI_CLK _LATB14
//************OLED Control*************//
#define DC_TRIS _TRISA0			//OUTPUT
#define DC _LATA0
#define OLED_RST_TRIS _TRISB13		//OUTPUT
#define OLED_RST _LATB13
#define OLED_SDIN_TRIS _TRISA1		//OUTPUT
#define OLED_SDIN _LATA1
#define OLED_SCLK_TRIS _TRISB15		//OUTPUT
#define OLED_SCLK _LATB15

//**************UART/I2C2***************//
#define TxD_SCL_TRIS _TRISB3		//OUTPUT
#define TxD_SCL _LATB3
#define Rxd_SDA_TRIS _TRISB2		//INPUT
#define Rxd_SDA _RB2

//******************ACM*****************//
#define ACM_SCL_TRIS _TRISB8		//UNKOWN
#define ACM_SCL _LATB8
#define ACM_SDA_TRIS _TRISB9		//UNKOWN
#define ACM_SDA _RB9
#define ACM_INT1_TRIS _TRISB4		//INPUT		
#define ACM_INT1 _RB4//_LATB4

//****************OTHERS*****************//
#define SW1_TRIS _TRISA3			//INPUT
#define SW1 _RA3
#define CLK32K_TRIS _TRISA4			//INPUT
#define CLK32K _RA4
#define IO_RA2_TRIS _TRISA2 
#define IO_RA2  _LATA2
#endif