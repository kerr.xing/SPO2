/* 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 作者              日期      
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 魏彬       		2014年7月30日	
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* 描述
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* SPI模块配置
**********************************************************************/

#include "p32mx150f128b.h"
#include "extern_func.h"
#include <stdio.h>
#include <plib.h>
#include "Port_init.h"

void initSPI2_Master(void);
void initSPI2_Master_EB(void);
void initSPI2_Master_EB_Disable(void);
void disableSPI2(void);
void PutcharSPI2_DATA(unsigned char c);
void PutarraySPI2_DATA(unsigned char array[],int n);
void PutcharSPI2_Command(unsigned char c);
void PutarraySPI2_Command(unsigned char array[],int n);

void initSPI1_Master(void);
void initSPI1_Master_EB(void);
void initSPI1_Master_EB_Disable(void);
void disableSPI1(void);
void PutcharSPI1_DATA(unsigned char c);
void PutarraySPI1_DATA(unsigned char array[],int n);


/****************************************
函数名：initSPI2_Master(void)
描述：SPI2初始化配置
**************************************/
void initSPI2_Master(void)
{	
 	SPI2CON = 0; // Stops and resets the SPI2.

 //	SPI2CONbits.FRZ=0;  //Freeze in Debug Exception Mode bit	//web
 	SPI2CONbits.SIDL=0;  //Stop in Idle Mode bit		//web
 	SPI2CONbits.DISSDO=0;   //Disable SDOx pin bit
 	SPI2CONbits.MODE32=0;   //32Bit Communication Select bits
 	SPI2CONbits.MODE16=0;   //16Bit Communication Select bits
 	SPI2CONbits.SMP=0;
 	SPI2CONbits.CKE=0;
 	SPI2CONbits.SSEN=0;  //Slave select enable (Slave mode only). 1=!SSx pin is used for slave mode, 0=used for I/O
 	SPI2CONbits.CKP=1;   //Clock polarity select. 1=idle state for clock is high, 0=idle is low	//web
 	SPI2CONbits.MSTEN=1; //1=master mode, 0=slave mode
	SPI2CONbits.DISSDI = 1;	//不使用SDI
 	SPI2CONbits.STXISEL=0b00;
 	SPI2CONbits.SRXISEL=0b00;

 	SPI2CONbits.FRMEN=0;  //1=frame support enable,0=disable
 	SPI2CONbits.FRMSYNC=0;  //Frame Sync Pulse Direction Control on SS pin 1=frame sync input(slave)
 	SPI2CONbits.FRMPOL=0;  //Frame Sync Polarity bit
 	SPI2CONbits.MSSEN=0;  //Master Mode Slave Select Enable bit
 	SPI2CONbits.FRMSYPW=0;   //Frame Sync Pulse Width bit
 	SPI2CONbits.SPIFE=0;  //Frame Sync Pulse Edge Select bit
 	SPI2CONbits.ENHBUF=0;  //Enhanced Buffer Enable bit

 	//SPI2BRG=2; 			//对应波特率5MHz
	SPI2BRG=0; 			//对应波特率2MHz
 	SPI2STATCLR=0x40; // clear the Overflow		//web
 	SPI2CONbits.ON=1;  //SPI Peripheral On bit

 //	char temp_char;
 	while (!SPI2STATbits.SPITBE);//发送缓冲区为空				//while (SPI2STATbits.SPIRBF)  //web
 //	{
 // 		temp_char=SPI2BUF;
 //	} 

}

//*******************SPI2 Enhanced buffer configure***********************//
void initSPI2_Master_EB(void)
{
 	SPI2CONbits.ENHBUF=1;  //Enhanced Buffer Enable bit
 	SPI2STATCLR=0x40; // clear the Overflow
}

//*******************SPI2 Enhanced buffer configure***********************//
void initSPI2_Master_EB_Disable(void)
{
 	SPI2CONbits.ENHBUF=0;  //Enhanced Buffer Enable bit
 	SPI2STATCLR=0x40; // clear the Overflow
}

//******************disable spi**********************//
void disableSPI2(void)
{
 	SPI2CON = 0; // Stops and resets the SPI2.
}

//******************SPI发送一个字节**********************//
void PutcharSPI2_DATA(unsigned char c)
{
	while(SPI2STATbits.SPITBF);
	DC = 1;
	SPI2BUF = c;
}

//******************SPI发送一个数组**********************//
void PutarraySPI2_DATA(unsigned char array[],int n)
{
	int i;
	for(i = 0;i < n;i++)
		PutcharSPI2_DATA(array[i]);
}
  



void PutcharSPI2_Command(unsigned char c)
{
	while(SPI2STATbits.SPITBF);
	DC = 0;
	SPI2BUF = c;
}

//******************SPI发送一个数组**********************//
void PutarraySPI2_Command(unsigned char array[],int n)
{
	int i;
	for(i = 0;i < n;i++)
		PutcharSPI2_Command(array[i]);
}




/****************************************
函数名：initSPI1_Master(void)
描述：SPI1初始化配置
**************************************/
void initSPI1_Master(void)
{	

 	SPI1CON = 0; // Stops and resets the SPI1.

	IEC1bits.SPI1RXIE = 0;
	IFS1bits.SPI1RXIF = 0;
	IPC7bits.SPI1IP = 6;
	IPC7bits.SPI1IS = 1;
	IFS1bits.SPI1RXIF = 0;
	IEC1bits.SPI1RXIE = 1;

 //	SPI1CONbits.FRZ=0;  //Freeze in Debug Exception Mode bit	//web
 	SPI1CONbits.SIDL=1;  //Stop in Idle Mode bit		//web
 	SPI1CONbits.DISSDO=0;   //Disable SDOx pin bit
 	SPI1CONbits.MODE32=1;   //32Bit Communication Select bits
 	SPI1CONbits.MODE16=1;   //16Bit Communication Select bits
 	SPI1CONbits.SMP=1;		//
 	SPI1CONbits.CKE=1;
 	SPI1CONbits.SSEN=0;  //Slave select enable (Slave mode only). 1=!SSx pin is used for slave mode, 0=used for I/O
 	SPI1CONbits.CKP=0;   //Clock polarity select. 1=idle state for clock is high, 0=idle is low	//web
 	SPI1CONbits.MSTEN=1; //1=master mode, 0=slave mode
	SPI1CONbits.DISSDI = 0;	//使用SDI
 	SPI1CONbits.STXISEL=0b00;
 	SPI1CONbits.SRXISEL=0b01;		//WEB 要改  非空就中断

 	SPI1CONbits.FRMEN=0;  //1=frame support enable,0=disable
 	SPI1CONbits.FRMSYNC=0;  //Frame Sync Pulse Direction Control on SS pin 1=frame sync input(slave)
 	SPI1CONbits.FRMPOL=0;  //Frame Sync Polarity bit
 	SPI1CONbits.MSSEN=0;  //Master Mode Slave Select Enable bit
 	SPI1CONbits.FRMSYPW=0;   //Frame Sync Pulse Width bit
 	SPI1CONbits.SPIFE=0;  //Frame Sync Pulse Edge Select bit
 	SPI1CONbits.ENHBUF=1;  //Enhanced Buffer Enable bit

 	//SPI1BRG=7; 			//对应波特率2MHz
	SPI1BRG=0; 			//对应波特率2MHz
 	SPI1STATCLR=0x40; // clear the Overflow		//web
 	SPI1CONbits.ON=1;  //SPI Peripheral On bit

 //	char temp_char;
 	while (!SPI1STATbits.SPITBE);//发送缓冲区为空				//while (SPI2STATbits.SPIRBF)  //web
 //	{
 // 		temp_char=SPI1BUF;
 //	} 

}

//*******************SPI1 Enhanced buffer configure***********************//
void initSPI1_Master_EB(void)
{
 	SPI1CONbits.ENHBUF=1;  //Enhanced Buffer Enable bit
 	SPI1STATCLR=0x40; // clear the Overflow
}

//*******************SPI1 Enhanced buffer configure***********************//
void initSPI1_Master_EB_Disable(void)
{
 	SPI1CONbits.ENHBUF=0;  //Enhanced Buffer Enable bit
 	SPI1STATCLR=0x40; // clear the Overflow
}

//******************disable spi**********************//
void disableSPI1(void)
{
 	SPI1CON = 0; // Stops and resets the SPI1.
}

//******************SPI发送一个字节**********************//
void PutcharSPI1_DATA(unsigned char c)
{
	while(SPI1STATbits.SPITBF);
	SPI1BUF = c;
}

//******************SPI发送一个数组**********************//
void PutarraySPI1_DATA(unsigned char array[],int n)
{
	int i;
	for(i = 0;i < n;i++)
		PutcharSPI1_DATA(array[i]);
}
